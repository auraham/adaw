# include <stdio.h>
# include <stdlib.h>
# include <math.h>
#include <string.h>

# include "Global.h" 
# include "Random.h"
# include "Problem.h"

void rectangle_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-10000;
        max_realvar[i]=10000;
	}
}

void MPDMP_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-10000;
        max_realvar[i]=10000;
	}
}


void sch1_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-100;
        max_realvar[i]=100;
	}
}

void sch2_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-5;
        max_realvar[i]=10;
	}
}

void fon1_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-4;
        max_realvar[i]=4;
	}
}

void fon2_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-4;
        max_realvar[i]=4;
	}
}

void kur_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-5;
        max_realvar[i]=5;
	}
}

void pol_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-3.1415926;
        max_realvar[i]=3.1415926;
	}
}

void bel_space(double *min_realvar, double *max_realvar)
{
		min_realvar[0]=0;
        max_realvar[0]=5;
		min_realvar[1]=0;
        max_realvar[1]=3;
}

void bnh_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-15;
        max_realvar[i]=30;
	}
}

void osy_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0]=0;
	max_realvar[0]=10;
	min_realvar[1]=0;
	max_realvar[1]=10;
	min_realvar[2]=1;
	max_realvar[2]=5;
	min_realvar[3]=0;
	max_realvar[3]=6;
	min_realvar[4]=1;
	max_realvar[4]=5;
	min_realvar[5]=0;
	max_realvar[5]=10;
}

void tnk_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=3.15;
	}
}

void srn_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-20;
        max_realvar[i]=20;
	}
}

void constrEX_space(double *min_realvar, double *max_realvar)
{
		min_realvar[0]=0.1;
        max_realvar[0]=1;
		min_realvar[1]=0;
        max_realvar[1]=5;
}

void water(double *min_realvar, double *max_realvar)
{
	min_realvar[0]=0.01;
	max_realvar[0]=0.45;
	min_realvar[1]=0.01;
	max_realvar[1]=0.1;
	min_realvar[2]=0.01;
	max_realvar[2]=0.1;
}

void miettinen_space(double *min_realvar, double *max_realvar)
{
		min_realvar[0]=-4.9;
        max_realvar[0]=3.2;
		min_realvar[1]=-3.5;
        max_realvar[1]=6;
}


void vnt1_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-2;
        max_realvar[i]=2;
	}
}

void vnt2_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-4;
        max_realvar[i]=4;
	}
}

void vnt3_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-3;
        max_realvar[i]=3;
	}
}

void vnt4_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=-4;
        max_realvar[i]=4;
	}
}

void zdt1_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void zdt2_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void zdt3_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void zdt4_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	for (int i=1; i<nreal; i++)
    {
		min_realvar[i]=-5;
        max_realvar[i]=5;
	}
}

void zdt6_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void mop1_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz1_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz1Inverted_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz1Scaled_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz2_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz2Inverted_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}


void dtlz2Convex_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz2Scaled_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz2InvertedConvex_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz3_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz3Convex_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz4_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz5_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz5I_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz6_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}

void dtlz7_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0;
        max_realvar[i]=1;
	}
}


void uf1_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	for (int i=1; i<nreal; i++)
    {
		min_realvar[i]=-1.0;
        max_realvar[i]=1.0;        
    }
}

void uf2_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	for (int i=1; i<nreal; i++)
    {
		min_realvar[i]=-1.0;
        max_realvar[i]=1.0;        
    }
}

void uf3_space(double *min_realvar, double *max_realvar)
{
	for (int i=0; i<nreal; i++)
    {
		min_realvar[i]=0.0;
        max_realvar[i]=1.0;        
    }
}

void uf4_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	for (int i=1; i<nreal; i++)
    {
		min_realvar[i]=-2.0;
        max_realvar[i]=2.0;        
    }
}

void uf5_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	for (int i=1; i<nreal; i++)
    {
		min_realvar[i]=-1.0;
        max_realvar[i]=1.0;        
    }
}

void uf6_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	for (int i=1; i<nreal; i++)
    {
		min_realvar[i]=-1.0;
        max_realvar[i]=1.0;        
    }
}

void uf7_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	for (int i=1; i<nreal; i++)
    {
		min_realvar[i]=-1.0;
        max_realvar[i]=1.0;        
    }
}

void uf8_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	min_realvar[1] = 0.0;
	max_realvar[1] = 1.0;
	for (int i=2; i<nreal; i++)
    {
		min_realvar[i]=-2.0;
        max_realvar[i]=2.0;        
    }
}

void uf9_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	min_realvar[1] = 0.0;
	max_realvar[1] = 1.0;
	for (int i=2; i<nreal; i++)
    {
		min_realvar[i]=-2.0;
        max_realvar[i]=2.0;        
    }
}

void uf10_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	min_realvar[1] = 0.0;
	max_realvar[1] = 1.0;
	for (int i=2; i<nreal; i++)
    {
		min_realvar[i]=-2.0;
        max_realvar[i]=2.0;        
    }
}

void cf8_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	min_realvar[1] = 0.0;
	max_realvar[1] = 1.0;
	for (int i=2; i<nreal; i++)
    {
		min_realvar[i]=-4.0;
        max_realvar[i]=4.0;        
    }
}

void cf9_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	min_realvar[1] = 0.0;
	max_realvar[1] = 1.0;
	for (int i=2; i<nreal; i++)
    {
		min_realvar[i]=-2.0;
        max_realvar[i]=2.0;        
    }
}

void cf10_space(double *min_realvar, double *max_realvar)
{
	min_realvar[0] = 0.0;
	max_realvar[0] = 1.0;
	min_realvar[1] = 0.0;
	max_realvar[1] = 1.0;
	for (int i=2; i<nreal; i++)
    {
		min_realvar[i]=-2.0;
        max_realvar[i]=2.0;        
    }
}