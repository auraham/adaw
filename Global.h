/* This file contains the variable and function declarations */
#pragma once
# define INF 1.0e14
# define EPS 1.0e-14
# define E  2.71828182845905
# define PI 3.14159265358979
#include"stdio.h"
# include "string.h"

typedef struct lists
{
    int index;
    struct lists *parent;
    struct lists *child;
} list;

typedef struct
{
    double constr_violation;
    double *xreal;
    int **gene;
    double *xbin;
    double *obj;
    double *constr;

    int nweight;					// the number of weight vectors corresponding to this individual
	list *weight_index;				// all the weight vectors that correspond to this individual
	int best_weight;				// the most suitable weight vector of this individual 

	double crowd_degree;			
	list *niche_neighbor;			
	double *obj_norm;				
} individual;


typedef struct
{
    int ind_index;					// the individual that this weight vector corresponds to
    double *weight_vector;							
	int *neighbor;					
	double fvalue;					// scalar value of the individual this weight vector corresponds to 
	double *fTch;					// TCH scalar values of all objectives of the individual this weight vector corresponds to
	int active;						// is this weight vector active in current population 
} weightVectors;


typedef struct
{
    individual *ind;
} population;


typedef struct lists2
{
    double *obj;
    struct lists2 *parent;
    struct lists2 *child;
} list2;

typedef struct ind_lists
{
    individual *ind;
    struct ind_lists *parent;
    struct ind_lists *child;
} ind_list;

extern int nreal;
extern int nbin;
extern int nobj;
extern int ncon;
extern int popsize;
extern int archive_size;
extern int archive_capacity;
extern double pcross_real;
extern double pcross_bin;
extern double pmut_real;
extern double pmut_bin;
extern double eta_c;
extern double eta_m;
extern int ngen;
extern int nbinmut;
extern int nrealmut;
extern int nbincross;
extern int nrealcross;
extern int *nbits;
extern double *min_realvar;
extern double *max_realvar;
extern double *min_binvar;
extern double *max_binvar;
extern int bitlength;
extern double *min_pf;
extern double *max_pf;
extern int I_number;
extern int weight_size;
extern double *ideal_point;
extern double prob;
extern int nr;
extern int neval;
extern int currenteval;
extern void (*test_problem)(double *, double *, int **, double *, double *);


void allocate_memory_pop (population *pop, int size);
void allocate_memory_ind (individual *ind);
void deallocate_memory_pop (population *pop, int size);
void deallocate_memory_ind (individual *ind);
void allocate_memory_weight (weightVectors *weight);
void deallocate_memory_weight (weightVectors *weight);

double maximum (double a, double b);
double minimum (double a, double b);
double determine_radius(double **c, int size, int k_closest);

void crossover (individual *parent1, individual *parent2, individual *child1, individual *child2);
void realcross (individual *parent1, individual *parent2, individual *child1, individual *child2);
void bincross (individual *parent1, individual *parent2, individual *child1, individual *child2);

int check_dominance (individual *a, individual *b);

void normalize_archive (population *pop, int size);
void normalize_pop (population *pop, int *index, int size);
void normalize_bothpop (population *archive_pop, int archive_size, population *pop, int popsize);

void evaluate_ind (individual *ind);

void initialization (population *pop, ind_list *archive);
void initialize_ind (individual *ind);


void insert (list *node, int x);
list* del (list *node);
list* findnode(list *node, int index);
void insert_ind (ind_list *node, individual *ind);
ind_list* del_ind (ind_list *node);

void copy_ind (individual *ind1, individual *ind2);

void mutation_ind (individual *ind);
void bin_mutate_ind (individual *ind);
void real_mutate_ind (individual *ind);

void report_obj (population *pop, FILE *fpt);
void report_time(double time, FILE *fpt);
void report_variable(population *pop, FILE *fpt);

int combination (int n, int m);
double distanceVector(double *x, double *y);
void minfastsort(double x[], int idx[], int n, int m);
double norm_vector(double *x);
double innerproduct(double *vec1, double *vec2);
double Tchebycheff(double *obj, double *namda);


void init_weight(int index_obj, int h, double **totalweight, int pre_sum, int* temp_weight);
void permutation(double **totalweight);
void initialize_neighbourhood(weightVectors *weight, int niche);
void update_neighbourhood(weightVectors *weight, int niche, int size);
void findUpdate_neighborhood(weightVectors *weight, int weightIndex, population *pop, int niche);
void update_problem(weightVectors *weight, population *pop, individual *ind, int indID, int weightID, int niche, int type, int *index2);
void update_reference(individual *ind);
void variation_update (weightVectors *weight, population *pop, int niche, ind_list *archive);
void remove_weight(weightVectors *weight, individual *ind);
void remove_weights(weightVectors *weight, population *pop);
void initialize_match (weightVectors *weight, population *pop);
void update_match(weightVectors *weight, int weightIndex, population *pop, int oldID, int newID);
void exchange_ind(weightVectors *weight, population *pop, int oldID, int newID);
void determine_weight(weightVectors *weight, individual *ind);
void associate_weightWithInd(weightVectors *weight, int weightIndex, individual *ind, int indIndex);
void remove_weightInInd(individual *ind, int indIndex, weightVectors *weight, int weightIndex);

void clear_pop(population *pop);
void clear_archive(ind_list *archive);
void clear_weight(weightVectors *weight, int size);

void update_archive (ind_list *archive, individual *ind);
void maintain_archive(ind_list *archive);
int add_weights(ind_list *archive, population *pop, weightVectors *weight, int niche);
void maintain_pop(weightVectors *weight, population *pop, int size);
int reorganize_pop(weightVectors *weight, population *pop, int size);



