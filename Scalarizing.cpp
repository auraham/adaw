# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Tchebycheff function */
double Tchebycheff(double *obj, double *namda)
{
	int j;
	double fvalue = 0;
	double max_fun;
	double diff, feval;
    
	max_fun = -1 * INF;
	for(j=0; j<nobj; j++)
	{
		diff = fabs(obj[j] - ideal_point[j]);

		if(namda[j]==0)
		{
			feval = diff/0.00001;
		}		
		else
		{
			feval = diff/namda[j];		
		}
		if(feval>max_fun) max_fun = feval;
	}

	return max_fun;
}
