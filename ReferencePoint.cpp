# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Function to update the reference point */
void update_reference(individual *ind)
{
	int i;

	for(i=0; i<nobj; i++)    
	{
		if(ind->obj[i] < ideal_point[i] + 0.0001)
		{
			ideal_point[i]  = ind->obj[i] - 0.0001;
		}		
	}

}