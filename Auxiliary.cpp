/* Some utility functions */

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Function to return the maximum of two variables */
double maximum (double a, double b)
{
    if (a>b)
    {
        return(a);
    }
    return (b);
}

/* Function to return the minimum of two variables */
double minimum (double a, double b)
{
    if (a<b)
    {
        return (a);
    }
    return (b);
}


int combination (int n, int m)
{
	int i;
	int result1 = 1;
	int result2 = 1;
	if (m > n-m)
		m = n - m;
	for (i=0; i<m; i++)
	{
		result1 *= (n - i);
		result2 *= (i+1);
	}
	return result1 / result2;
}

double distanceVector(double *x, double *y)
{
	int i;
    double sum = 0;

	for(i=0; i<nobj; i++)
	    sum += (x[i] - y[i]) * (x[i] - y[i]);

	return sqrt(sum);
}

double norm_vector(double *x)
{
	int i;
	double sum = 0;
	for(i=0; i<nobj; i++)
        sum = sum + x[i]*x[i];
    return sqrt(sum);
}

double innerproduct(double *vec1, double *vec2)
{
	int i;
    double sum = 0;
	for(i=0; i<nobj; i++)
		sum+= vec1[i] * vec2[i];
	return sum;
}

void minfastsort(double x[], int idx[], int n, int m)
{
    int i, j;
	double temp;
	int id;
	
	for(i=0; i<m; i++)
	{
	    for(j=i+1; j<n; j++)
			if (x[i]>x[j])
			{
			    temp		= x[i];
				x[i]        = x[j];
				x[j]        = temp;
				id		    = idx[i];
				idx[i]      = idx[j];
				idx[j]      = id;
			}
	}
	return;
}


/* Function to copy an individual 'ind1' into another individual 'ind2' */
void copy_ind (individual *ind1, individual *ind2)
{
    int i, j;
    
    ind2->constr_violation = ind1->constr_violation;
    if (nreal!=0)
    {
        for (i=0; i<nreal; i++)
        {
            ind2->xreal[i] = ind1->xreal[i];
        }
    }
    if (nbin!=0)
    {
        for (i=0; i<nbin; i++)
        {
            ind2->xbin[i] = ind1->xbin[i];
            for (j=0; j<nbits[i]; j++)
            {
                ind2->gene[i][j] = ind1->gene[i][j];
            }
        }
    }
    for (i=0; i<nobj; i++)
    {
        ind2->obj[i] = ind1->obj[i];
    }
    if (ncon!=0)
    {
        for (i=0; i<ncon; i++)
        {
            ind2->constr[i] = ind1->constr[i];
        }
    }
    return;
}

/* Function to determine the radius of the niche */
double determine_radius(double **c, int size, int k_closest)
{
	int i, j, l, m;
	double **closest_dist;			// record the distance of the individual to its kth closest individual 
	double *ave_dist;				// record the average distance of an individual to its kth closest individual in the population
	double radius;
	double *x;
	int *idx, median, median2;


	closest_dist = (double **)malloc(size*sizeof(double*));
	for (i=0; i<size; i++)
    {
        closest_dist[i] = (double *)malloc(k_closest*sizeof(double));
    }
	ave_dist = (double *)malloc(k_closest*sizeof(double));

	for (i=0; i<size; i++)
    {
        for (j=0; j<k_closest; j++)
		{
			closest_dist[i][j] = INF;
		}
    }

	for (i=0; i<size; i++)
	{   
		for(j=i+1; j<size; j++)
		{
			for(l=0; l<k_closest; l++)
			{
				if (closest_dist[i][l] > c[i][j])
				{
					for(m=k_closest-1; m>l; m--)
					{
						closest_dist[i][m] = closest_dist[i][m-1];
					}
					closest_dist[i][l] = c[i][j];
					break;
				}
			}
			for(l=0; l<k_closest; l++)
			{
				if (closest_dist[j][l] > c[i][j])
				{
					for(m=k_closest-1; m>l; m--)
					{
						closest_dist[j][m] = closest_dist[j][m-1];
					}
					closest_dist[j][l] = c[i][j];
					break;
				}
			}
		}
	}

// find the median of the kth closest distance of all individuals
	x = (double *)malloc(size*sizeof(double));
	idx = (int *)malloc(size*sizeof(int));
	for(i=0; i<size; i++)
	{
		x[i]    = closest_dist[i][k_closest - 1];
		idx[i]  = i;
	}
	minfastsort(x, idx, size, size);
	if (size % 2 == 0)
	{
		median = size/2 - 1;
		median2 = size/2;
		radius = (x[median] + x[median2])/2;
	}
	else
	{
		median = (size - 1)/2;
		radius = x[median];
	}
	free (x);
	free (idx);

	
	for (i=0; i<size; i++)
    {
        free (closest_dist[i]);
    }
    free (closest_dist);

	return radius;
}
