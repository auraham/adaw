# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Routine to initialize a population, and update the reference point and the archive set */ 
void initialization (population *pop, ind_list *archive)
{
    int i;
    for (i=0; i<popsize; i++)
    {
        initialize_ind (&(pop->ind[i]));
		evaluate_ind(&(pop->ind[i]));
		update_reference(&(pop->ind[i]));
		update_archive(archive, &(pop->ind[i]));
    }
    return;
}

/* Function to initialize an individual randomly */
void initialize_ind (individual *ind)
{
    int j, k;
    if (nreal!=0)
    {
        for (j=0; j<nreal; j++)
        {
            ind->xreal[j] = rndreal (min_realvar[j], max_realvar[j]);
        }
    }
    if (nbin!=0)
    {
        for (j=0; j<nbin; j++)
        {
            for (k=0; k<nbits[j]; k++)
            {
                if (randomperc() <= 0.5)
                {
                    ind->gene[j][k] = 0;
                }
                else
                {
                    ind->gene[j][k] = 1;
                }
            }
        }
    }
    return;
}
