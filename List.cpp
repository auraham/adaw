# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Insert an element X into the list at location specified by NODE */
void insert (list *node, int x)
{
    list *temp;
    if (node==NULL)
    {
        printf("\n Error!! asked to enter after a NULL pointer, hence exiting \n");
        exit(1);
    }
    temp = (list *)malloc(sizeof(list));
    temp->index = x;
    temp->child = node->child;
    temp->parent = node;
    if (node->child != NULL)
    {
        node->child->parent = temp;
    }
    node->child = temp;
    return;
}

/* Delete the node NODE from the list */
list* del (list *node)
{
    list *temp;
    if (node==NULL)
    {
        printf("\n Error!! asked to delete a NULL pointer, hence exiting \n");
        exit(1);
    }
    temp = node->parent;
    temp->child = node->child;
    if (temp->child!=NULL)
    {
        temp->child->parent = temp;
    }
    free (node);
    return (temp);
}

/* Find an element INDEX from the list NODE */
list* findnode(list *node, int index)
{
	list *temp;
    if (node==NULL)
    {
        printf("\n Error!! this node is the final point in this list! \n");
        exit(1);
    }
	temp=node;
	while (temp!=NULL && temp->index!=index)
	{
		temp=temp->child;
	}
	if (temp==NULL)
	{
		printf("\n cannot find this point! \n");
	}
	return temp;
}

/* Insert an element IND into the individual list at location specified by NODE */
void insert_ind (ind_list *node, individual *ind)
{
    ind_list *temp_ind;
    if (node==NULL)
    {
        printf("\n Error!! asked to enter after a NULL pointer, hence exiting \n");
        exit(1);
    }
    temp_ind = (ind_list *)malloc(sizeof(ind_list));
    temp_ind->ind = (individual *)malloc(sizeof(individual));
    allocate_memory_ind (temp_ind->ind);
    copy_ind (ind, temp_ind->ind);
    temp_ind->child = node->child;
    temp_ind->parent = node;
    if (node->child != NULL)
    {
        node->child->parent = temp_ind;
    }
    node->child = temp_ind;
	archive_size++;
    return;
}

/* Delete the node NODE from the individual list */
ind_list* del_ind (ind_list *node)
{
    ind_list *temp_ind;
    if (node==NULL)
    {
        printf("\n Error!! asked to delete a NULL pointer, hence exiting \n");
        exit(1);
    }
    temp_ind = node->parent;
    temp_ind->child = node->child;
    if (temp_ind->child!=NULL)
    {
        temp_ind->child->parent = temp_ind;
    }
    deallocate_memory_ind(node->ind);
    free (node->ind);
    free (node);
	archive_size--;
    return (temp_ind);
}

