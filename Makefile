CC=g++
CFLAGS=-c -Wall -g

all: main

# --- main ---

main:        main.o Allocation.o Auxiliary.o ClearUp.o Crossover.o Dominance.o Evaluation.o Global.o Initialization.o List.o MOEADevolution.o Mutation.o Normalization.o OpsReArchive.o OpsReNeighborhood.o OpsRePopulation.o OpsReWeight.o Problem.o Random.o ReferencePoint.o Report.o Scalarizing.o VariableRange.o
	$(CC) -g main.o Allocation.o Auxiliary.o ClearUp.o Crossover.o Dominance.o Evaluation.o Global.o Initialization.o List.o MOEADevolution.o Mutation.o Normalization.o OpsReArchive.o OpsReNeighborhood.o OpsRePopulation.o OpsReWeight.o Problem.o Random.o ReferencePoint.o Report.o Scalarizing.o VariableRange.o -lm -o demo

# --- dependencies ---
main.o:
	$(CC) $(CFLAGS) main.cpp

Allocation.o:
	$(CC) $(CFLAGS) Allocation.cpp -o Allocation.o

Auxiliary.o:
	$(CC) $(CFLAGS) Auxiliary.cpp -o Auxiliary.o

ClearUp.o:
	$(CC) $(CFLAGS) ClearUp.cpp -o ClearUp.o

Crossover.o:
	$(CC) $(CFLAGS) Crossover.cpp -o Crossover.o

Dominance.o:
	$(CC) $(CFLAGS) Dominance.cpp -o Dominance.o

Evaluation.o:
	$(CC) $(CFLAGS) Evaluation.cpp -o Evaluation.o

Global.o:
	$(CC) $(CFLAGS) Global.cpp -o Global.o

Initialization.o:
	$(CC) $(CFLAGS) Initialization.cpp -o Initialization.o

List.o:
	$(CC) $(CFLAGS) List.cpp -o List.o

MOEADevolution.o:
	$(CC) $(CFLAGS) MOEADevolution.cpp -o MOEADevolution.o

Mutation.o:
	$(CC) $(CFLAGS) Mutation.cpp -o Mutation.o

Normalization.o:
	$(CC) $(CFLAGS) Normalization.cpp -o Normalization.o

OpsReArchive.o:
	$(CC) $(CFLAGS) OpsReArchive.cpp -o OpsReArchive.o

OpsReNeighborhood.o:
	$(CC) $(CFLAGS) OpsReNeighborhood.cpp -o OpsReNeighborhood.o

OpsRePopulation.o:
	$(CC) $(CFLAGS) OpsRePopulation.cpp -o OpsRePopulation.o

OpsReWeight.o:
	$(CC) $(CFLAGS) OpsReWeight.cpp -o OpsReWeight.o

Problem.o:
	$(CC) $(CFLAGS) Problem.cpp -o Problem.o

Random.o:
	$(CC) $(CFLAGS) Random.cpp -o Random.o

ReferencePoint.o:
	$(CC) $(CFLAGS) ReferencePoint.cpp -o ReferencePoint.o

Report.o:
	$(CC) $(CFLAGS) Report.cpp -o Report.o

Scalarizing.o:
	$(CC) $(CFLAGS) Scalarizing.cpp -o Scalarizing.o

VariableRange.o:
	$(CC) $(CFLAGS) VariableRange.cpp -o VariableRange.o

# --- clean ---
clean:
	rm *.o demo

