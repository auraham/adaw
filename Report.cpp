# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Function to report the decision variable of the population */
void report_variable(population *pop, FILE *fpt)
{
    int i, j;

	for (i=0; i<popsize; i++)
	{
		for (j=0; j<nreal-1; j++)
		{
			fprintf(fpt,"%e\t",pop->ind[i].xreal[j]);
		}
		fprintf(fpt,"%e\t\n",pop->ind[i].xreal[nreal-1]);  
   	}
	fprintf(fpt, "\n");
	return;
}

/* Function to report the running time of the algorithm */
void report_time(double time, FILE *fpt)
{
	fprintf(fpt,"%f\n",time);
}

/* Function to report the objective values of the population */
void report_obj(population *pop, FILE *fpt)
{
	int i, j;
	for (i=0; i<popsize; i++)
	{
		for (j=0; j<nobj-1; j++)
		{
			fprintf(fpt,"%e\t",pop->ind[i].obj[j]);
		}
		fprintf(fpt,"%e\t\n",pop->ind[i].obj[nobj-1]);
	}
	fprintf(fpt, "\n");
}

