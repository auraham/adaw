# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Routine for evolutionary operations of an MOEA/D algorithm (here Li and Zhang, 2009) */
void variation_update (weightVectors *weight, population *pop, int niche, ind_list *archive)
{
	int i, j;
	int r1, r2, p1, p2;
	individual *child;
	int *index;
	int type;
	list *temp;
	
	child = (individual *)malloc(sizeof(individual));
	allocate_memory_ind(child);
	index = (int*)malloc(weight_size*sizeof(int));

	j=0;
	for (i=0; i<popsize; i++)
	{
		temp = pop->ind[i].weight_index->child;
		while (temp!=NULL)
		{
			index[j] = temp->index;
			j++;
			temp = temp->child;
		}
	}

	for (i=0; i<weight_size; i++)
	{
		if (rndreal(0,1)<prob)
			type = 1;
		else 
			type = 2;

		if (type == 1)
		{
			r1 = rnd(0, niche-1);
			do{
				r2 = rnd(0, niche-1);
			} 
			while (r2 == r1);
			p1 = weight[index[i]].neighbor[r1];
			p2 = weight[index[i]].neighbor[r2];
		}
		else
		{
			p1 = index[rnd(0, weight_size-1)];
			do{
				p2 = index[rnd(0, weight_size-1)];
			} 
			while (p2 == p1);
		}
		if (p1 != index[i])
			crossover(&pop->ind[weight[index[i]].ind_index], &pop->ind[weight[p1].ind_index], &pop->ind[i+popsize], child);
		else
			crossover(&pop->ind[weight[index[i]].ind_index], &pop->ind[weight[p2].ind_index], &pop->ind[i+popsize], child);

		mutation_ind(&pop->ind[i+popsize]);
		evaluate_ind(&pop->ind[i+popsize]);
		update_archive (archive, &pop->ind[i+popsize]);						// for each new solution, check if it can enter the archive set
		update_reference(&pop->ind[i+popsize]);
		update_problem(weight, pop, &pop->ind[i+popsize], i+popsize, index[i], niche, type, index);
	}

// exchange individuals to make sure that the front individuals are active (i.e., correspond to at least one weight vector)
	popsize = reorganize_pop(weight, pop, popsize+weight_size);


	deallocate_memory_ind(child);
	free(child);
	free(index);

    return;
}

