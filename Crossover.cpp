# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Function to cross two individuals */
void crossover (individual *parent1, individual *parent2, individual *child1, individual *child2)
{
    if (nreal!=0)
    {
		realcross (parent1, parent2, child1, child2);
    }
    if (nbin!=0)
    {
        bincross (parent1, parent2, child1, child2);
    }
    return;
}


/* Function for the SBX crossover */
void realcross (individual *parent1, individual *parent2, individual *child1, individual *child2)
{
    int i;
	double c1, c2;
    double beta;
	double mu;

	if (randomperc() <= pcross_real)
    {
		for (i=0; i<nreal; i++)
        {
            if (randomperc() <= 0.5 )
			{
				mu = randomperc();
				if (mu <= 0.5)
					beta = pow((2*mu),1.0/(eta_c+1.0));
				else
					beta = pow((2*(1-mu)),-1.0/(eta_c+1.0));
				c1 = ((1.0 + beta) * parent1->xreal[i] + (1.0 - beta) * parent2->xreal[i])/2.0;
				c2 = ((1.0 - beta) * parent1->xreal[i] + (1.0 + beta) * parent2->xreal[i])/2.0;
				if (randomperc()<=0.5)
				{
					child1->xreal[i] = c2;
					child2->xreal[i] = c1;
				}
				else
				{
					child1->xreal[i] = c1;
					child2->xreal[i] = c2;
				}

				if (child1->xreal[i]<min_realvar[i])
				{
					child1->xreal[i]=min_realvar[i];
				}
				if (child1->xreal[i]>max_realvar[i])
				{
					child1->xreal[i]=max_realvar[i];
				}
				if (child2->xreal[i]<min_realvar[i])
				{
					child2->xreal[i]=min_realvar[i];
				}
				if (child2->xreal[i]>max_realvar[i])
				{
					child2->xreal[i]=max_realvar[i];
				}
			}
			else
			{
				child1->xreal[i] = parent1->xreal[i];
				child2->xreal[i] = parent2->xreal[i];
			}
		}
	}
	else
	{
		for (i=0; i<nreal; i++)
		{
			child1->xreal[i] = parent1->xreal[i];
			child2->xreal[i] = parent2->xreal[i];
		}
	}
	return;
}

/* Function for two point binary crossover */
void bincross (individual *parent1, individual *parent2, individual *child1, individual *child2)
{
    int i, j;
    double rand;
    int temp, site1, site2;
    for (i=0; i<nbin; i++)
    {
        rand = randomperc();
        if (rand <= pcross_bin)
        {
            nbincross++;
            site1 = rnd(0,nbits[i]-1);
            site2 = rnd(0,nbits[i]-1);
            if (site1 > site2)
            {
                temp = site1;
                site1 = site2;
                site2 = temp;
            }
            for (j=0; j<site1; j++)
            {
                child1->gene[i][j] = parent1->gene[i][j];
                child2->gene[i][j] = parent2->gene[i][j];
            }
            for (j=site1; j<site2; j++)
            {
                child1->gene[i][j] = parent2->gene[i][j];
                child2->gene[i][j] = parent1->gene[i][j];
            }
            for (j=site2; j<nbits[i]; j++)
            {
                child1->gene[i][j] = parent1->gene[i][j];
                child2->gene[i][j] = parent2->gene[i][j];
            }
        }
        else
        {
            for (j=0; j<nbits[i]; j++)
            {
                child1->gene[i][j] = parent1->gene[i][j];
                child2->gene[i][j] = parent2->gene[i][j];
            }
        }
    }
    return;
}
