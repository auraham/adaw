# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Function to initialize the neighborhood of weight vectors based on their Euclidean distances */
void initialize_neighbourhood(weightVectors *weight, int niche)
{
    int i, j, k;
	double *x;
	int *idx;
	

	x = (double *)malloc(weight_size*sizeof(double));
	idx = (int *)malloc(weight_size*sizeof(int));

	for(i=0; i<weight_size; i++)
	{	
		for(j=0; j<weight_size; j++)
		{
			x[j]    = distanceVector(weight[i].weight_vector, weight[j].weight_vector);
			idx[j]  = j;			
		}
		minfastsort(x, idx, weight_size, niche);

		for(k=0; k<niche; k++)
			weight[i].neighbor[k] = idx[k];
	}
    free (x);
	free (idx);
	return;

}

/* Function to update the neighborhood of weight vectors based on their Euclidean distances */
void update_neighbourhood(weightVectors *weight, int niche, int size)
{
    int i, j, k;
	double *x;
	int *idx, *index;
	int count;
	
	index = (int *)malloc(size*sizeof(int));

	count = 0;
	for(i=0; i<size; i++)
	{
		if (weight[i].active == 1)
		{
			index[count] = i;
			count ++;
		}
	}

	x = (double *)malloc(count*sizeof(double));
	idx = (int *)malloc(count*sizeof(int));

	for(i=0; i<count; i++)
	{	
		for(j=0; j<count; j++)
		{
			x[j]    = distanceVector(weight[index[i]].weight_vector, weight[index[j]].weight_vector);
			idx[j]  = index[j];
		}
		minfastsort(x, idx, count, niche);

		for(k=0; k<niche; k++)
			weight[index[i]].neighbor[k] = idx[k];	
	}
    free (x);
	free (idx);
	free (index);
	return;
}

/* Function to find and update the neighborhood of weight vectors */
void findUpdate_neighborhood(weightVectors *weight, int weightIndex, population *pop, int niche)
{
	int j, k;
	double *x;
	int *idx, *index;
	int count;
	double f1, f2;
	double sum1, sum2;
	int success = 1;				// to flag whether the weight vector will be kept in the population or not
	list *temp;
	
// find the neighbors of the weight vector
	index = (int *)malloc((weight_size+archive_capacity)*sizeof(int));
	count = 0;
	for (j=0; j<popsize; j++)
	{
		temp = pop->ind[j].weight_index->child;
		while (temp!=NULL)
		{
			index[count] = temp->index;
			count++;
			temp = temp->child;
		}
	}
	x = (double *)malloc(count*sizeof(double));
	idx = (int *)malloc(count*sizeof(int));
	for(j=0; j<count; j++)
	{
		x[j]    = distanceVector(weight[weightIndex].weight_vector, weight[index[j]].weight_vector);
		idx[j]  = index[j];
	}
	minfastsort(x, idx, count, niche);

	
// update information of the weight vector according to its neighbors
	for (k=0; k<niche; k++)
	{
		f2 = Tchebycheff(pop->ind[weight[idx[k]].ind_index].obj, weight[weightIndex].weight_vector);
		if (f2 < weight[weightIndex].fvalue)
		{
			remove_weight(&weight[weightIndex], &pop->ind[weight[weightIndex].ind_index]);
			success = 0;
			break;
		}
		else
		{
			if (f2 == weight[idx[k]].fvalue)
			{
				sum1 = 0;
				sum2 = 0;
				for (j=0; j<nobj; j++)
				{
					sum1 += pop->ind[weight[weightIndex].ind_index].obj[j];
					sum2 += pop->ind[weight[idx[k]].ind_index].obj[j];
				}
				if (sum2 < sum1)  
				{
					remove_weight(&weight[weightIndex], &pop->ind[weight[weightIndex].ind_index]);
					success = 0;
					break;
				}
			}
		}
	}

// update information of the neighbors of the weight vector
	if (success == 1)
	{
		for(k=0; k<niche; k++)
		{
			f1 = Tchebycheff(pop->ind[weight[weightIndex].ind_index].obj, weight[idx[k]].weight_vector);
			if (f1 < weight[idx[k]].fvalue)
			{
				update_match(weight, idx[k], pop, weight[idx[k]].ind_index, weight[weightIndex].ind_index);
			}
			else
			{
				if (f1 == weight[idx[k]].fvalue)
				{
					sum1 = 0;
					sum2 = 0;
					for (j=0; j<nobj; j++)
					{
						sum1 += pop->ind[weight[idx[k]].ind_index].obj[j];
						sum2 += pop->ind[weight[weightIndex].ind_index].obj[j];
					}
					if (sum2 < sum1) 
					{
						update_match(weight, idx[k], pop, weight[idx[k]].ind_index, weight[weightIndex].ind_index);
					}
				}
			}
		}
	}

    free (x);
	free (idx);
	free (index);
	return;
}


/* Function to update the solutions of neighboring subproblems (weight vectors) in MOEA/D */
void update_problem(weightVectors *weight, population *pop, individual *ind, int indID, int weightID, int niche, int type, int *index2)
{
	int i, j;
	int index;
	double f1, f2;
	double sum1, sum2;
	int size, times=0, rand, temp;
	int *perm;


	if(type == 1)	
		size = niche;              // from neighborhood
	else        
		size = weight_size;        // from whole population

	perm = (int *)malloc(size*sizeof(int));
	for (i=0; i<size; i++)
		perm[i] = i;

	//shuffle randomly for permutation;
	for (i=0; i<size; i++)
    {
        rand = rnd (i, size-1);
        temp = perm[rand];
        perm[rand] = perm[i];
        perm[i] = temp;
    }

	for(i=0; i<size; i++)
	{
		// pick a subproblem to update
		if(type == 1) 
			index = weight[weightID].neighbor[perm[i]];
		else        
			index = index2[perm[i]];

		f1 = Tchebycheff(pop->ind[weight[index].ind_index].obj, weight[index].weight_vector);
		f2 = Tchebycheff(ind->obj, weight[index].weight_vector);
		if(f2 < f1)
		{
			// update the individual that the weight vector corresponds to
			update_match(weight, index, pop, weight[index].ind_index, indID);
			times ++;
		}
		if (f2 == f1)
		{
			sum1 = 0;
			sum2 = 0;
			for (j=0; j<nobj; j++)
			{
				sum1 += pop->ind[weight[index].ind_index].obj[j];
				sum2 += ind->obj[j];
			}
			if (sum2 < sum1)
			{
				// update the individual that the weight vector corresponds to
				update_match(weight, index, pop, weight[index].ind_index, indID);
				times++;
			}
		}
		if(times > nr)
			break;
	}

	free(perm);

	return;
}

