# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Function to update the archive set by an individual 'ind' */
void update_archive (ind_list *archive, individual *ind)
{
    int flag;
    ind_list *temp_ind;

	if (archive->child == NULL)
	{
		insert_ind (archive, ind);
	}
	else
	{
		temp_ind = archive->child;
		do
		{
			flag = check_dominance (ind, temp_ind->ind);
			switch (flag)
			{
			case 1: /* ind dominates temp_ind->ind */
				{
					temp_ind = del_ind (temp_ind);
					temp_ind = temp_ind->child;
					break;
				}
			case -1: /* temp_ind->ind dominates ind */
				{
					return;
				}
			case 0: /* they are non-dominated */
				{
					temp_ind = temp_ind->child;
					break;
				}
			}
		}
		while (temp_ind!=NULL);   
		insert_ind(archive, ind);
	}
    return;
}


/* Function to maintain the archive set by removing the most crowded solutions */
void maintain_archive(ind_list *archive)
{
	int i, j, k;
	ind_list *temp_ind;
	int current_size, original_size;
	int index;
	int *mark;
	list *temp, *temp2;
	double radius, distance, max_crowding;
	double **c;
	population *pop;

	pop = (population *)malloc(sizeof(population));
	allocate_memory_pop (pop, archive_size);
	
	
	c = (double **)malloc(archive_size*sizeof(double*));  
	for (i=0; i<archive_size; i++)
    {
        c[i] = (double *)malloc(archive_size*sizeof(double));
    }

// copy the list archive into a pop for ease of operation 
	temp_ind = archive->child;
	i = 0;
	while (temp_ind!=NULL)
	{
		copy_ind (temp_ind->ind, &(pop->ind[i]));
		temp_ind = temp_ind->child;
		i++;
	}

//	normalise the archive 
	normalize_archive(pop, archive_size);

	for (i=0; i<archive_size; i++)
	{   
		for(j=i+1; j<archive_size; j++)
		{
			distance = 0;
			for(k=0; k<nobj; k++)
			{
			    distance += (pop->ind[i].obj_norm[k]-pop->ind[j].obj_norm[k]) * (pop->ind[i].obj_norm[k]-pop->ind[j].obj_norm[k]);
			}
			distance = sqrt(distance);
            c[i][j] = distance;
            c[j][i] = distance;
		}
		c[i][i] = INF;
	}

//	calculate the radius for the archive maintenance
	radius = determine_radius(c, archive_size, nobj);
	

//	use "mark" to record which idividuals should be removed
	mark = (int *)malloc(archive_size*sizeof(int));
	for (i=0; i<archive_size; i++)
	{

		mark[i] = 1;								// "1" means that the individual is in the current archive set
		pop->ind[i].crowd_degree = 1;				// initialisation of individuals' crowding degree
		temp = pop->ind[i].niche_neighbor->child;	// initialisation of individuals' neighbor in their niche
		while(temp != NULL)
		{
			temp = del(temp);
			temp = temp->child;
		}
	}

// find neighbors and calculate the crowding degree
	for (i=0; i<archive_size; i++)
	{   
		for(j=i+1; j<archive_size; j++)
		{
			if (c[i][j] < radius)
			{
				pop->ind[i].crowd_degree *= c[i][j]/radius;
				pop->ind[j].crowd_degree *= c[i][j]/radius;

				insert(pop->ind[i].niche_neighbor, j);
				insert(pop->ind[j].niche_neighbor, i);
			}
		}
	}
	for (i=0; i<archive_size; i++)
	{
		pop->ind[i].crowd_degree = 1.0 - pop->ind[i].crowd_degree;
	}


	current_size = archive_size;
	do
	{
// find the individual with the highest crowding degree in the current archive 
		max_crowding = -1;
		for (i=0; i<archive_size; i++)
		{
			if (mark[i] == 1)
			{
				if (pop->ind[i].crowd_degree > max_crowding)
				{
					max_crowding = pop->ind[i].crowd_degree;
					index = i;
				}
			}
		}

		if (max_crowding == 0)			// this means that all the remaining individuals are not neighboring to each other
		{
			// in this case, randomly remove some until the archive size reduces to the capacity
			while(current_size > archive_capacity)
			{
				do
				{
					index = rnd(0, archive_size-1);
				} 
				while(mark[index] == 0);
				mark[index] = 0;
				current_size--;
			}
		}
		else
		{
			mark[index] = 0;					// "0" means that individual "index" is removed from the archive 	
// renew the information of the neighbors of individual "index"; 
// this includes removing individual "index" from their neighbor list and updating their crowding degree  
			temp = pop->ind[index].niche_neighbor->child;
			while (temp != NULL)
			{
// remove individual "index" from the neighbor list
				temp2 = findnode(pop->ind[temp->index].niche_neighbor->child, index);
				temp2 = del(temp2);

// update the crowding degree for two situations, i.e. whether the two individuals are overlapping
				if (c[index][temp->index] != 0)
				{
					pop->ind[temp->index].crowd_degree = 1.0 - (1.0-pop->ind[temp->index].crowd_degree) / (c[index][temp->index]/radius);
				}
				else
				{
					pop->ind[temp->index].crowd_degree = 1.0;
					temp2 = pop->ind[temp->index].niche_neighbor->child;
					while (temp2 != NULL)
					{
						pop->ind[temp->index].crowd_degree *= c[temp2->index][temp->index]/radius;
						temp2 = temp2->child;
					}
					pop->ind[temp->index].crowd_degree = 1.0 - pop->ind[temp->index].crowd_degree;
				}
				temp = temp->child;
			}
			current_size--;
		}
	}
	while(current_size > archive_capacity);

// renew the archive 
	original_size = archive_size;
	temp_ind = archive->child;
	while (temp_ind != NULL)
	{
		temp_ind = del_ind(temp_ind);
		temp_ind = temp_ind->child;
	}
	for (i=0; i<original_size; i++)
	{
		if (mark[i] == 1)
		{
			insert_ind(archive, &pop->ind[i]);
		}
	}
	free(mark);
	for (i=0; i<original_size; i++)
    {
        free (c[i]);
    }
    free (c);

	deallocate_memory_pop (pop, original_size);
    free (pop);

	return;
}
