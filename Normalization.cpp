# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Function to normalise the archive set */
void normalize_archive (population *pop, int size)
{
	int i, j;
	double *max, *min;

	max = (double *)malloc(nobj*sizeof(double));
	min = (double *)malloc(nobj*sizeof(double));

	for (j=0; j<nobj; j++)
	{
		min[j] = INF;
		max[j] = -INF;
        for (i=0; i<size; i++)
		{
			if (pop->ind[i].obj[j]<min[j])
			{
				min[j] = pop->ind[i].obj[j];
			}
			if (pop->ind[i].obj[j]>max[j])
			{
                max[j] = pop->ind[i].obj[j];
			}				
		}
		
		if (max[j] == min[j])
		{
			for (i=0; i<size; i++)
			{
				pop->ind[i].obj_norm[j] = 0.0;
			}
		}
		else
		{
			for (i=0; i<size; i++)
			{
				pop->ind[i].obj_norm[j] = (pop->ind[i].obj[j] - min[j]) / (max[j] - min[j]);
			}
		}
	}
	free(max);
	free(min);
	return;
}

/* Function to normalise the population */
void normalize_pop (population *pop, int *index, int size)
{
	int i, j;
	double *max, *min;

	max = (double *)malloc(nobj*sizeof(double));
	min = (double *)malloc(nobj*sizeof(double));

	for (j=0; j<nobj; j++)
	{
		min[j] = INF;
		max[j] = -INF;
        for (i=0; i<size; i++)
		{
			if (pop->ind[index[i]].obj[j]<min[j])
			{
				min[j] = pop->ind[index[i]].obj[j];
			}
			if (pop->ind[index[i]].obj[j]>max[j])
			{
                max[j] = pop->ind[index[i]].obj[j];
			}				
		}
		
		if (max[j] == min[j])
		{
			for (i=0; i<size; i++)
			{
				pop->ind[index[i]].obj_norm[j] = 0.0;
			}
		}
		else
		{
			for (i=0; i<size; i++)
			{
				pop->ind[index[i]].obj_norm[j] = (pop->ind[index[i]].obj[j] - min[j]) / (max[j] - min[j]);
			}
		}
	}
	free(max);
	free(min);
	return;
}


/* Function to normalise the poulation and the archive set according to the archive set */ 
void normalize_bothpop (population *archive_pop, int archive_size, population *pop, int popsize)
{
	int i, j;
	
	double *max, *min;
	max = (double *)malloc(nobj*sizeof(double));
	min = (double *)malloc(nobj*sizeof(double));

	for (j=0; j<nobj; j++)
	{
		min[j] = INF;
		max[j] = -INF;
        for (i=0; i<archive_size; i++)
		{
			if (archive_pop->ind[i].obj[j]<min[j])
			{
				min[j] = archive_pop->ind[i].obj[j];
			}
			if (archive_pop->ind[i].obj[j]>max[j])
			{
                max[j] = archive_pop->ind[i].obj[j];
			}				
		}
		
		if (max[j] == min[j])
		{
			for (i=0; i<archive_size; i++)
			{
				archive_pop->ind[i].obj_norm[j] = 0.0;
			}
			for (i=0; i<popsize; i++)
			{
				pop->ind[i].obj_norm[j] = pop->ind[i].obj[j] - min[j];
			}
		}
		else
		{
			for (i=0; i<archive_size; i++)
			{
				archive_pop->ind[i].obj_norm[j] = (archive_pop->ind[i].obj[j] - min[j]) / (max[j] - min[j]);
			}
			for (i=0; i<popsize; i++)
			{
				pop->ind[i].obj_norm[j] = (pop->ind[i].obj[j] - min[j]) / (max[j] - min[j]);
			}
		}
	}
	free(max);
	free(min);
	return;
}





