# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Function to clear up the population */
void clear_pop(population *pop)
{
	int i;
	list *temp;

	for (i=0; i<popsize; i++)
	{
		if (pop->ind[i].nweight > 0)
		{
			pop->ind[i].best_weight = -1;
			pop->ind[i].nweight = 0;
			temp = pop->ind[i].weight_index->child;
			while (temp!=NULL)
			{
				temp = del(temp);
				temp = temp->child;
			}
		}
		else
		{
			printf("should be greater than 0!!\n");
			exit(1);
		}
	}
	return;
}

/* Function to clear up the weight vectors */
void clear_weight(weightVectors *weight, int size)
{
	int i;

	for (i=0; i<size; i++)
	{
		weight[i].ind_index = -1;
		weight[i].active = 0;
		weight[i].fvalue = INF;
	}
  return;
}

/* Function to clear up the archive set */
void clear_archive(ind_list *archive)
{
	ind_list *temp_ind;

	temp_ind = archive->child;
	while (temp_ind!=NULL)
	{
		temp_ind = del_ind(temp_ind);
		temp_ind = temp_ind->child;
	}
	return;
}