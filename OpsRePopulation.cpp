# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"


/* Function to exchange individuals to make sure that the first popsize individuals are active 
i.e., they are associated with at least one weight vector */
int reorganize_pop(weightVectors *weight, population *pop, int size)
{
	int i, j;
	int current_size;

	j = 0;
	for (i=size-1; i>=0; i--)
	{
		if (pop->ind[i].nweight > 0)
		{
			while (pop->ind[j].nweight != 0)
			{
				j++;
			}
			if (i>j)
			{
				exchange_ind(weight, pop, i, j);
				j++;
			}
			else
			{
				current_size = j;
				break;
			}
		}
	}

	return current_size;
}

/* Routine to find the most crowded solutions in the population, and remove them and the corresponding weight vectors */
void maintain_pop(weightVectors *weight, population *pop, int size)
{
	int i, j, k;
	int current_size;
	int *position;
	list *temp, *temp2;
	double radius, distance, max_crowding;
	double **c;
	int index;

	c = (double **)malloc(size*sizeof(double*));  
	for (i=0; i<size; i++)
    {
        c[i] = (double *)malloc(size*sizeof(double));
    }
	position = (int *)malloc(size*sizeof(int));

// record the individuals who only correspond to one weight vector 
	j = 0;
	for (i=0; i<popsize; i++)
    {
		if (pop->ind[i].nweight == 1)
		{
			position[j] = i;
			j++;
		}
    }

//	normalise the poulation 
	normalize_pop(pop, position, size);

	for (i=0; i<size; i++)
	{   
		for(j=i+1; j<size; j++)
		{
			distance = 0;
			for(k=0; k<nobj; k++)
			{
			    distance += (pop->ind[position[i]].obj_norm[k]-pop->ind[position[j]].obj_norm[k]) * (pop->ind[position[i]].obj_norm[k]-pop->ind[position[j]].obj_norm[k]);
			}
			distance = sqrt(distance);
            c[i][j] = distance;
            c[j][i] = distance;
		}
		c[i][i] = INF;
	}

//	calculate the radius for population maintenance
	radius = determine_radius(c, size, nobj);

	for (i=0; i<size; i++)
	{
		pop->ind[position[i]].crowd_degree = 1;				// initialisation of individuals' crowding degree
		temp = pop->ind[position[i]].niche_neighbor->child;	// initialisation of individuals' neighbor in their niche
		while(temp != NULL)
		{
			temp = del(temp);
			temp = temp->child;
		}
	}

// find solutions' neighbors and calculate the crowding degree
	for (i=0; i<size; i++)
	{   
		for(j=i+1; j<size; j++)
		{
			if (c[i][j] < radius)
			{
				pop->ind[position[i]].crowd_degree *= c[i][j]/radius;
				pop->ind[position[j]].crowd_degree *= c[i][j]/radius;

				insert(pop->ind[position[i]].niche_neighbor, j);
				insert(pop->ind[position[j]].niche_neighbor, i);
			}
		}
	}
	for (i=0; i<size; i++)
	{
		pop->ind[position[i]].crowd_degree = 1.0 - pop->ind[position[i]].crowd_degree;
	}

	current_size = size;
	do
	{
// find the individual with the highest crowding degree in the population
		max_crowding = -1;
		for (i=0; i<size; i++)
		{
			if (pop->ind[position[i]].nweight > 0)
			{
				if (pop->ind[position[i]].crowd_degree > max_crowding)
				{
					max_crowding = pop->ind[position[i]].crowd_degree;
					index = i;
				}
			}
		}

		if (max_crowding == 0)			// this means that all the remaining individuals are not neighboring to each other
		{
// in this case, randomly remove some until the number of weight vectors reduces to the weight_size
			while(current_size > weight_size)
			{
				do
				{
					index = rnd(0, size-1);
				} 
				while(pop->ind[position[index]].nweight == 0);

				remove_weight(&weight[pop->ind[position[index]].weight_index->child->index], &pop->ind[position[index]]);
				current_size --;
			}
		}
		else
		{
// remove the weight vector associated with the most crowded individual
			remove_weight(&weight[pop->ind[position[index]].weight_index->child->index], &pop->ind[position[index]]);

// renew the information of the neighbors of the most crowded individual, 
// this includes removing the most crowded individual from its neighbors' neighbor list and updating its neighbors' crowding degree  
			temp = pop->ind[position[index]].niche_neighbor->child;
			while (temp != NULL)
			{
// remove the most crowded individual from the neighbor list
				temp2 = findnode(pop->ind[position[temp->index]].niche_neighbor->child, index);
				temp2 = del(temp2);

// update the crowding degree for two situations, i.e. whether the two individuals are overlapping or not
				if (c[index][temp->index] != 0)
				{
					pop->ind[position[temp->index]].crowd_degree = 1.0 - (1.0-pop->ind[position[temp->index]].crowd_degree) / (c[index][temp->index]/radius);
				}
				else
				{
					pop->ind[position[temp->index]].crowd_degree = 1.0;
					temp2 = pop->ind[position[temp->index]].niche_neighbor->child;
					while (temp2 != NULL)
					{
						pop->ind[position[temp->index]].crowd_degree *= c[temp2->index][temp->index]/radius;
						temp2 = temp2->child;
					}
					pop->ind[position[temp->index]].crowd_degree = 1.0 - pop->ind[position[temp->index]].crowd_degree;
				}
				temp = temp->child;
			}
			current_size--;
		}
	}
	while(current_size > weight_size);

	for (i=0; i<size; i++)
    {
        free (c[i]);
    }
    free (c);
	free(position);

	return;
}