# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <string.h>

# include "Global.h" 
# include "Random.h"
# include "Problem.h"



void (*test_problem)(double *, double *, int **, double *, double *);

/*  Test problem Water_Resource
	# of real variables = 3
	# of bin variables = 0
	# of objective = 5
	# of constraints = 7
	range x1 [0.01, 0.45], x2,x3 [0.01, 0.10]
*/
void water(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	obj[0] = 106780.37 * (xreal[1] + xreal[2]) + 61704.67;
	obj[1] = 3000 * xreal[0];
	obj[2] = (305700 / pow(0.06*2289, 0.65)) * 2289 * xreal[1];
	obj[3] = 250 * 2289 * pow(E, -39.75*xreal[1] + 9.9*xreal[2] + 2.74);
	obj[4] = 25 * (1.39 / (xreal[0] * xreal[1]) + 4940 * xreal[2] - 80);

	obj[0] = obj[0] / 80000;
	obj[1] = obj[1] / 1500;
	obj[2] = obj[2] / 3000000;
	obj[3] = obj[3] / 6000000;
	obj[4] = obj[4] / 8000;

	constr[0] = 1 - (0.00139 / (xreal[0] * xreal[1]) + 4.94 * xreal[2] - 0.08);
	constr[1] = 0.1 - (0.0000306 / (xreal[0] * xreal[1]) + 0.1082 * xreal[2] - 0.00986);
	constr[2] = 50000 - (12.307 / (xreal[0] * xreal[1]) + 49408.24 * xreal[2] - 4051.02);
	constr[3] = 16000 - (2.098 / (xreal[0] * xreal[1]) + 8046.33 * xreal[2] - 696.71);
	constr[4] = 10000 - (2.138 / (xreal[0] * xreal[1]) + 7883.39 * xreal[2] - 705.04);
	constr[5] = 2000 - (0.417 / (xreal[0] * xreal[1]) + 1721.36 * xreal[2] - 136.54);
	constr[6] = 550 - (0.164 / (xreal[0] * xreal[1]) + 631.13 * xreal[2] - 54.48);

	return;
}


/*  Test problem ishibuchi
	# of real variables = 2
	# of bin variables = 0
	# of objective = any
	# of constraints = 0
	range x1[0, 1000], x2[0, 1000]
*/
void MPDMP (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{    

	obj[0] = sqrt((xreal[0] - 50)*(xreal[0] - 50) + (xreal[1] - 88)*(xreal[1] - 88));
	obj[1] = sqrt((xreal[0] - 70)*(xreal[0] - 70) + (xreal[1] - 80)*(xreal[1] - 80));
	obj[2] = sqrt((xreal[0] - 83)*(xreal[0] - 83) + (xreal[1] - 62)*(xreal[1] - 62));
	obj[3] = sqrt((xreal[0] - 83)*(xreal[0] - 83) + (xreal[1] - 38)*(xreal[1] - 38));
	obj[4] = sqrt((xreal[0] - 70)*(xreal[0] - 70) + (xreal[1] - 20)*(xreal[1] - 20));
	obj[5] = sqrt((xreal[0] - 50)*(xreal[0] - 50) + (xreal[1] - 12)*(xreal[1] - 12));
	obj[6] = sqrt((xreal[0] - 30)*(xreal[0] - 30) + (xreal[1] - 20)*(xreal[1] - 20));
	obj[7] = sqrt((xreal[0] - 17)*(xreal[0] - 17) + (xreal[1] - 38)*(xreal[1] - 38));	
	obj[8] = sqrt((xreal[0] - 17)*(xreal[0] - 17) + (xreal[1] - 62)*(xreal[1] - 62));
	obj[9] = sqrt((xreal[0] - 30)*(xreal[0] - 30) + (xreal[1] - 80)*(xreal[1] - 80));
	
    return;
}

/*  Test problem Square
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 4
    # of constraints = 0
	range x1[-10000, 10000], x2[-10000, 10000]
*/
void rectangle (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	/*obj[0] = 100 > xreal[1] ? 100 - xreal[1] : xreal[1] - 100; 
	obj[1] = xreal[0] > 0 ? xreal[0] : -xreal[0];
	obj[2] = xreal[1] > 0 ? xreal[1] : -xreal[1];
	obj[3] = 100 > xreal[0] ? 100 - xreal[0] : xreal[0] - 100;*/


	obj[0] = fabs(100 - xreal[1]);
	obj[1] = fabs(xreal[0] - 0);
	obj[2] = fabs(xreal[1] - 0);
	obj[3] = fabs(100 - xreal[0]);

    return;
}


/*  Test problem miettinen
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 5
    # of constraints = 0
	range x1[-4.9, 3.2], x2[-3.5, 6]
*/

double u1 (double x1, double x2)
{
	return (3*(1-x1)*(1-x1)*pow(E, -1*(x1*x1+(x2+1)*(x2+1))));
}

double u2 (double x1, double x2)
{
	return (-10*(x1/4 - pow(x1, 3) - pow(x2, 5))*pow(E, -1*(x1*x1+x2*x2)));
}

double u3 (double x1, double x2)
{
	return (1/3 * pow (E, -1*((x1+1)*(x1+1)+x2*x2)));
} 

void miettinen (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{    
	double x1, x2;
	x1 = xreal[0];
	x2 = xreal[1];
	obj[0] = -1*(u1(x1, x2) + u2(x1, x2) + u3(x1, x2)) + 10;
    obj[1] = -1*(u1(x1-1.2, x2-1.5) + u2(x1-1.2, x2-1.5) + u3(x1-1.2, x2-1.5)) + 10;
	obj[2] = -1*(u1(x1+0.3, x2-3.0) + u2(x1+0.3, x2-3.0) + u3(x1+0.3, x2-3.0)) + 10;
	obj[3] = -1*(u1(x1-1.0, x2+0.5) + u2(x1-1.0, x2+0.5) + u3(x1-1.0, x2+0.5)) + 10;
	obj[4] = -1*(u1(x1-0.5, x2-1.7) + u2(x1-0.5, x2-1.7) + u3(x1-0.5, x2-1.7)) + 10;
    return;
}



/*  Test problem SCH1
    # of real variables = 1
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
	range [-100, 100]
    */

void sch1 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    obj[0] = pow(xreal[0],2.0);
    obj[1] = pow((xreal[0]-2.0),2.0);
    return;
}


/*  Test problem SCH2
    # of real variables = 1
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
	range [-5, 10]
	*/
void sch2 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    if (xreal[0]<=1.0)
    {
        obj[0] = -xreal[0];
        obj[1] = pow((xreal[0]-5.0),2.0);
        return;
    }
    if (xreal[0]<=3.0)
    {
        obj[0] = xreal[0]-2.0;
        obj[1] = pow((xreal[0]-5.0),2.0);
        return;
    }
    if (xreal[0]<=4.0)
    {
        obj[0] = 4.0-xreal[0];
        obj[1] = pow((xreal[0]-5.0),2.0);
        return;
    }
    obj[0] = xreal[0]-4.0;
    obj[1] = pow((xreal[0]-5.0),2.0);
    return;
}


/*  Test problem FON1
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
	range [-4, 4]
    */


void fon1 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double s1, s2;
    s1 = -1.0*pow((xreal[0]-1), 2.0)-pow((xreal[1]+1), 2.0);
	s2 = -1.0*pow((xreal[0]+1), 2.0)-pow((xreal[1]-1), 2.0);
    obj[0] = 1.0 - exp(s1);
    obj[1] = 1.0 - exp(s2);
    return;
}

/*  Test problem FON2
    # of real variables = 3
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
	range [-4, 4]
    */


void fon2 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double s1, s2;
    int i;
    s1 = s2 = 0.0;
    for (i=0; i<nreal; i++)
    {
        s1 += pow((xreal[i]-(1.0/sqrt((double)nreal))),2.0);
        s2 += pow((xreal[i]+(1.0/sqrt((double)nreal))),2.0);
    }
    obj[0] = 1.0 - exp(-s1);
    obj[1] = 1.0 - exp(-s2);
    return;
}


/*  Test problem KUR
    # of real variables = 3
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
	range [-5, 5]
    */


void kur (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    int i;
    double res1, res2;
    res1 = -0.2*sqrt((xreal[0]*xreal[0]) + (xreal[1]*xreal[1]));
    res2 = -0.2*sqrt((xreal[1]*xreal[1]) + (xreal[2]*xreal[2]));
    obj[0] = -10.0*( exp(res1) + exp(res2));
    obj[1] = 0.0;
    for (i=0; i<3; i++)
    {
        obj[1] += pow(fabs(xreal[i]),0.8) + 5.0*sin(pow(xreal[i],3.0));
    }
    return;
}


/*  Test problem POL
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
	range [-3.1415926, 3.1415926]
    */


void pol (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double a1, a2, b1, b2;
    a1 = 0.5*sin(1.0) - 2.0*cos(1.0) + sin(2.0) - 1.5*cos(2.0);
    a2 = 1.5*sin(1.0) - cos(1.0) + 2.0*sin(2.0) - 0.5*cos(2.0);
    b1 = 0.5*sin(xreal[0]) - 2.0*cos(xreal[0]) + sin(xreal[1]) - 1.5*cos(xreal[1]);
    b2 = 1.5*sin(xreal[0]) - cos(xreal[0]) + 2.0*sin(xreal[1]) - 0.5*cos(xreal[1]);
    obj[0] = 1.0 + pow((a1-b1),2.0) + pow((a2-b2),2.0);
    obj[1] = pow((xreal[0]+3.0),2.0) + pow((xreal[1]+1.0),2.0);
    return;
}


/*  Test problem VNT1
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 3
    # of constraints = 0
	range [-2, 2]

    */

void vnt1 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    obj[0] = xreal[0]*xreal[0] + pow((xreal[1]-1), 2.0);
	obj[1] = xreal[0]*xreal[0] + pow((xreal[1]+1), 2.0) + 1.0;
	obj[2] = pow((xreal[0]-1), 2.0) + xreal[1]*xreal[1] + 2.0;

    return;
}


/*  Test problem VNT2
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 3
    # of constraints = 0
	range [-4, 4]
    */

void vnt2 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    obj[0] = 0.5*pow((xreal[0]-2), 2.0) + (pow((xreal[1]+1), 2.0))/13.0 + 3.0;
	obj[1] = (pow((xreal[0] + xreal[1] - 3.0), 2.0))/36.0 +(pow((-1.0*xreal[0] + xreal[1] + 2.0), 2.0))/8.0 - 17.0;
	obj[2] = (pow((xreal[0] + 2.0*xreal[1] - 1.0), 2.0))/175.0 + (pow((-1.0*xreal[0] + 2.0*xreal[1]), 2.0))/17.0 - 13.0;
	
    return;
}


/*  Test problem VNT3
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 3
    # of constraints = 0
	range [-3, 3]
    */

void vnt3 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    obj[0] = 0.5*(xreal[0]*xreal[0] + xreal[1]*xreal[1]) + sin(xreal[0]*xreal[0] + xreal[1]*xreal[1]);
    obj[1] = (pow((3.0*xreal[0] - 2.0*xreal[1] + 4.0),2.0))/8.0 + (pow((xreal[0]-xreal[1]+1.0),2.0))/27.0 + 15.0;
    obj[2] = 1.0/(xreal[0]*xreal[0] + xreal[1]*xreal[1] + 1.0) - 1.1*exp(-(xreal[0]*xreal[0] + xreal[1]*xreal[1]));
    return;
}


/*  Test problem ZDT1
    # of real variables = 30
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */
void zdt1 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = xreal[0];
    g = 0.0;
    for (i=1; i<30; i++)
    {
        g += xreal[i];
    }
    g = 9.0*g/29.0;
    g += 1.0;
    h = 1.0 - sqrt(f1/g);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}


/*  Test problem ZDT2
    # of real variables = 30
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */


void zdt2 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = xreal[0];
    g = 0.0;
    for (i=1; i<30; i++)
    {
        g += xreal[i];
    }
    g = 9.0*g/29.0;
    g += 1.0;
    h = 1.0 - pow((f1/g),2.0);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}


/*  Test problem ZDT3
    # of real variables = 30
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */

void zdt3 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = xreal[0];
    g = 0.0;
    for (i=1; i<30; i++)
    {
        g += xreal[i];
    }
    g = 9.0*g/29.0;
    g += 1.0;
    h = 1.0 - sqrt(f1/g) - (f1/g)*sin(10.0*PI*f1);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}

/*  Test problem ZDT4
    # of real variables = 10
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */


void zdt4 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = xreal[0];
    g = 0.0;
    for (i=1; i<10; i++)
    {
        g += xreal[i]*xreal[i] - 10.0*cos(4.0*PI*xreal[i]);
    }
    g += 91.0;
    h = 1.0 - sqrt(f1/g);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}


/*  Test problem ZDT5
    # of real variables = 0
    # of bin variables = 11
    # of bits for binvar1 = 30
    # of bits for binvar2-11 = 5
    # of objectives = 2
    # of constraints = 0
    */

#ifdef zdt5
void test_problem (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    int i, j;
    int u[11];
    int v[11];
    double f1, f2, g, h;
    for (i=0; i<11; i++)
    {
        u[i] = 0;
    }
    for (j=0; j<30; j++)
    {
        if (gene[0][j] == 1)
        {
            u[0]++;
        }
    }
    for (i=1; i<11; i++)
    {
        for (j=0; j<4; j++)
        {
            if (gene[i][j] == 1)
            {
                u[i]++;
            }
        }
    }
    f1 = 1.0 + u[0];
    for (i=1; i<11; i++)
    {
        if (u[i] < 5)
        {
            v[i] = 2 + u[i];
        }
        else
        {
            v[i] = 1;
        }
    }
    g = 0;
    for (i=1; i<11; i++)
    {
        g += v[i];
    }
    h = 1.0/f1;
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}
#endif

/*  Test problem ZDT6
    # of real variables = 10
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */


void zdt6 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = 1.0 - (exp(-4.0*xreal[0]))*pow((sin(6.0*PI*xreal[0])),6.0);
    g = 0.0;
    for (i=1; i<10; i++)
    {
        g += xreal[i];
    }
    g = g/9.0;
    g = pow(g,0.25);
    g = 1.0 + 9.0*g;
    h = 1.0 - pow((f1/g),2.0);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}


/*  Test problem MOP1
    # of real variables = 10
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */


void mop1 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, t;
    int i;

	g = 0.0;
	for (i=1; i<nreal; i++)
	{
		t = xreal[i] - sin(0.5*PI*xreal[0]);
		g += -0.9*t*t + pow(abs(t),0.6);
	}
	g *= 2*sin(PI*xreal[0]);

	f1 = (1+g)*xreal[0];
	f2 = (1+g)*(1-pow(xreal[0],0.5));

    obj[0] = f1;
    obj[1] = f2;
    return;
}



/*  Test problem BEL
# of real variables = 2
# of bin variables = 0
# of objectives = 2
# of constraints = 2
*/


void bel (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	obj[0] = -2*xreal[0] + xreal[1];
	obj[1] = 2*xreal[0] + xreal[1];
	constr[0] = 1.0 - (-xreal[0]+xreal[1])/1.0;
	constr[1] = 1.0 - (xreal[0] + xreal[1])/7.0 ;
	return;
}


/*  Test problem BNH
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 2
    */


void bnh (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    obj[0] = 4.0*(xreal[0]*xreal[0] + xreal[1]*xreal[1]);
    obj[1] = pow((xreal[0]-5.0),2.0) + pow((xreal[1]-5.0),2.0);
    constr[0] = 1.0 - (pow((xreal[0]-5.0),2.0) + xreal[1]*xreal[1])/25.0;
    constr[1] = (pow((xreal[0]-8.0),2.0) + pow((xreal[1]+3.0),2.0))/7.7 - 1.0;
    return;
}


/*  Test problem OSY
    # of real variables = 6
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 6
    */


void osy (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    obj[0] = -(25.0*pow((xreal[0]-2.0),2.0) + pow((xreal[1]-2.0),2.0) + pow((xreal[2]-1.0),2.0) + pow((xreal[3]-4.0),2.0) + pow((xreal[4]-1.0),2.0));
    obj[1] = xreal[0]*xreal[0] +  xreal[1]*xreal[1] + xreal[2]*xreal[2] + xreal[3]*xreal[3] + xreal[4]*xreal[4] + xreal[5]*xreal[5];
    constr[0] = (xreal[0]+xreal[1])/2.0 - 1.0;
    constr[1] = 1.0 - (xreal[0]+xreal[1])/6.0;
    constr[2] = 1.0 - xreal[1]/2.0 + xreal[0]/2.0;
    constr[3] = 1.0 - xreal[0]/2.0 + 3.0*xreal[1]/2.0;
    constr[4] = 1.0 - (pow((xreal[2]-3.0),2.0))/4.0 - xreal[3]/4.0;
    constr[5] = (pow((xreal[4]-3.0),2.0))/4.0 + xreal[5]/4.0 - 1.0;
    return;
}


/*  Test problem SRN
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 2
    */


void srn (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    obj[0] = 2.0 + pow((xreal[0]-2.0),2.0) + pow((xreal[1]-1.0),2.0);
    obj[1] = 9.0*xreal[0] - pow((xreal[1]-1.0),2.0);
    constr[0] = 1.0 - (pow(xreal[0],2.0) + pow(xreal[1],2.0))/225.0;
    constr[1] = 3.0*xreal[1]/10.0 - xreal[0]/10.0 - 1.0;
    return;
}


/*  Test problem TNK
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 2
    */

void tnk (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    obj[0] = xreal[0];
    obj[1] = xreal[1];
    if (xreal[1] == 0.0)
    {
        constr[0] = -1.0;
    }
    else
    {
        constr[0] = xreal[0]*xreal[0] + xreal[1]*xreal[1] - 0.1*cos(16.0*atan(xreal[0]/xreal[1])) - 1.0;
    }
    constr[1] = 1.0 - 2.0*pow((xreal[0]-0.5),2.0) + 2.0*pow((xreal[1]-0.5),2.0);
    return;
}



void constrEX (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	obj[0] = xreal[0];
	obj[1] = (1+xreal[1])/xreal[0];
	constr[0] = xreal[1]+9*xreal[0]-6.0;
	constr[1] = -xreal[1]+9*xreal[0]-1.0;
	return;
}


/*  Test problem Viennet4
# of real variables = 2
# of bin variables = 0
# of objectives = 3
# of constraints = 3
*/


void vnt4 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	obj[0] = (xreal[0]-2.0)*(xreal[0]-2.0)/2.0 + (xreal[1]+1.0)*(xreal[1]+1.0)/13.0 + 3.0;
	obj[1] = (xreal[0]+ xreal[1]-3.0)*(xreal[0]+xreal[1]-3.0)/175.0 +(2.0*xreal[1]-xreal[0])*(2.0*xreal[1]-xreal[0])/17.0 -13.0;
	obj[2] = (3.0*xreal[0]-2.0*xreal[1]+4.0)*(3.0*xreal[0]-2.0*xreal[1]+4.0)/8.0 + (xreal[0]-xreal[1]+1.0)*(xreal[0]-xreal[1]+1.0)/27.0 + 15.0;
	constr[0] = -xreal[1] - (4.0 * xreal[0]) + 4.0  ;
	constr[1] = xreal[0] + 1.0 ;
	constr[2] = xreal[1] - xreal[0] + 2.0 ;	
	return;
}



/*  Test problem CF8
# of real variables = 10(in CEC09)
# of bin variables = 0
# of objectives = 3
# of constraints = 1
*/

void cf8(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	unsigned int j, count1, count2, count3;
	double sum1, sum2, sum3, yj, N, a;
	N = 2.0; a = 4.0;

	sum1   = sum2   = sum3   = 0.0;
	count1 = count2 = count3 = 0;
	for(j = 3; j <= nreal; j++) 
	{
		yj = xreal[j-1] - 2.0*xreal[1]*sin(2.0*PI*xreal[0]+j*PI/nreal);
		if(j % 3 == 1) 
		{
			sum1  += yj*yj;
			count1++;
		} 
		else if(j % 3 == 2) 
		{
			sum2  += yj*yj;
			count2++;
		}
		else
		{
			sum3  += yj*yj;
			count3++;
		}
	}
	obj[0] = cos(0.5*PI*xreal[0])*cos(0.5*PI*xreal[1]) + 2.0*sum1 / (double)count1;
	obj[1] = cos(0.5*PI*xreal[0])*sin(0.5*PI*xreal[1]) + 2.0*sum2 / (double)count2;
	obj[2] = sin(0.5*PI*xreal[0])                  + 2.0*sum3 / (double)count3;
	constr[0] = (obj[0]*obj[0]+obj[1]*obj[1])/(1-obj[2]*obj[2]) - a*fabs(sin(N*PI*((obj[0]*obj[0]-obj[1]*obj[1])/(1-obj[2]*obj[2])+1.0))) - 1.0;
}


/*  Test problem CF9
# of real variables = 10(in CEC09)
# of bin variables = 0
# of objectives = 3
# of constraints = 1
*/

void cf9(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	unsigned int j, count1, count2, count3;
	double sum1, sum2, sum3, yj, N, a;
	N = 2.0; a = 3.0;

	sum1   = sum2   = sum3   = 0.0;
	count1 = count2 = count3 = 0;
	for(j = 3; j <= nreal; j++) 
	{
		yj = xreal[j-1] - 2.0*xreal[1]*sin(2.0*PI*xreal[0]+j*PI/nreal);
		if(j % 3 == 1) 
		{
			sum1  += yj*yj;
			count1++;
		} 
		else if(j % 3 == 2) 
		{
			sum2  += yj*yj;
			count2++;
		}
		else
		{
			sum3  += yj*yj;
			count3++;
		}
	}
	obj[0] = cos(0.5*PI*xreal[0])*cos(0.5*PI*xreal[1]) + 2.0*sum1 / (double)count1;
	obj[1] = cos(0.5*PI*xreal[0])*sin(0.5*PI*xreal[1]) + 2.0*sum2 / (double)count2;
	obj[2] = sin(0.5*PI*xreal[0])                  + 2.0*sum3 / (double)count3;
	constr[0] = (obj[0]*obj[0]+obj[1]*obj[1])/(1-obj[2]*obj[2]) - a*sin(N*PI*((obj[0]*obj[0]-obj[1]*obj[1])/(1-obj[2]*obj[2])+1.0)) - 1.0;
}


/*  Test problem CF10
# of real variables = 10(in CEC09)
# of bin variables = 0
# of objectives = 3
# of constraints = 1
*/

void cf10(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	unsigned int j, count1, count2, count3;
	double sum1, sum2, sum3, yj, hj, N, a;
	N = 2.0; a = 1.0;

	sum1   = sum2   = sum3   = 0.0;
	count1 = count2 = count3 = 0;
	for(j = 3; j <= nreal; j++) 
	{
		yj = xreal[j-1] - 2.0*xreal[1]*sin(2.0*PI*xreal[0]+j*PI/nreal);
		
		hj = 4.0*yj*yj - cos(8.0*PI*yj) + 1.0;
		if(j % 3 == 1) 
		{
			sum1  += hj;
			count1++;
		} 
		else if(j % 3 == 2) 
		{
			sum2  += hj;
			count2++;
		}
		else
		{
			sum3  += hj;
			count3++;
		}
	}
	obj[0] = cos(0.5*PI*xreal[0])*cos(0.5*PI*xreal[1]) + 2.0*sum1 / (double)count1;
	obj[1] = cos(0.5*PI*xreal[0])*sin(0.5*PI*xreal[1]) + 2.0*sum2 / (double)count2;
	obj[2] = sin(0.5*PI*xreal[0])                  + 2.0*sum3 / (double)count3;
	constr[0] = (obj[0]*obj[0]+obj[1]*obj[1])/(1-obj[2]*obj[2]) - a*sin(N*PI*((obj[0]*obj[0]-obj[1]*obj[1])/(1-obj[2]*obj[2])+1.0)) - 1.0;
}




/*  Test problem CTP1
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 2
    */


void ctp1 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double g;
    g = 1.0 + xreal[1];
    obj[0] = xreal[0];
    obj[1] = g*exp(-obj[0]/g);
    constr[0] = obj[1]/(0.858*exp(-0.541*obj[0]))-1.0;
    constr[1] = obj[1]/(0.728*exp(-0.295*obj[0]))-1.0;
    return;
}


/*  Test problem CTP2
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 1
    */


void ctp2 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double g;
    double theta, a, b, c, d, e;
    double exp1, exp2;
    theta = -0.2*PI;
    a = 0.2;
    b = 10.0;
    c = 1.0;
    d = 6.0;
    e = 1.0;
    g = 1.0 + xreal[1];
    obj[0] = xreal[0];
    obj[1] = g*(1.0  - sqrt(obj[0]/g));
    exp1 = (obj[1]-e)*cos(theta) - obj[0]*sin(theta);
    exp2 = (obj[1]-e)*sin(theta) + obj[0]*cos(theta);
    exp2 = b*PI*pow(exp2,c);
    exp2 = fabs(sin(exp2));
    exp2 = a*pow(exp2,d);
    constr[0] = exp1/exp2 - 1.0;
    return;
}


/*  Test problem CTP3
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 1
    */


void ctp3 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double g;
    double theta, a, b, c, d, e;
    double exp1, exp2;
    theta = -0.2*PI;
    a = 0.1;
    b = 10.0;
    c = 1.0;
    d = 0.5;
    e = 1.0;
    g = 1.0 + xreal[1];
    obj[0] = xreal[0];
    obj[1] = g*(1.0  - sqrt(obj[0]/g));
    exp1 = (obj[1]-e)*cos(theta) - obj[0]*sin(theta);
    exp2 = (obj[1]-e)*sin(theta) + obj[0]*cos(theta);
    exp2 = b*PI*pow(exp2,c);
    exp2 = fabs(sin(exp2));
    exp2 = a*pow(exp2,d);
    constr[0] = exp1/exp2 - 1.0;
    return;
}


/*  Test problem CTP4
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 1
    */


void ctp4 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double g;
    double theta, a, b, c, d, e;
    double exp1, exp2;
    theta = -0.2*PI;
    a = 0.75;
    b = 10.0;
    c = 1.0;
    d = 0.5;
    e = 1.0;
    g = 1.0 + xreal[1];
    obj[0] = xreal[0];
    obj[1] = g*(1.0  - sqrt(obj[0]/g));
    exp1 = (obj[1]-e)*cos(theta) - obj[0]*sin(theta);
    exp2 = (obj[1]-e)*sin(theta) + obj[0]*cos(theta);
    exp2 = b*PI*pow(exp2,c);
    exp2 = fabs(sin(exp2));
    exp2 = a*pow(exp2,d);
    constr[0] = exp1/exp2 - 1.0;
    return;
}


/*  Test problem CTP5
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 1
    */


void ctp5 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double g;
    double theta, a, b, c, d, e;
    double exp1, exp2;
    theta = -0.2*PI;
    a = 0.1;
    b = 10.0;
    c = 2.0;
    d = 0.5;
    e = 1.0;
    g = 1.0 + xreal[1];
    obj[0] = xreal[0];
    obj[1] = g*(1.0  - sqrt(obj[0]/g));
    exp1 = (obj[1]-e)*cos(theta) - obj[0]*sin(theta);
    exp2 = (obj[1]-e)*sin(theta) + obj[0]*cos(theta);
    exp2 = b*PI*pow(exp2,c);
    exp2 = fabs(sin(exp2));
    exp2 = a*pow(exp2,d);
    constr[0] = exp1/exp2 - 1.0;
    return;
}


/*  Test problem CTP6
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 1
    */


void ctp6 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double g;
    double theta, a, b, c, d, e;
    double exp1, exp2;
    theta = 0.1*PI;
    a = 40.0;
    b = 0.5;
    c = 1.0;
    d = 2.0;
    e = -2.0;
    g = 1.0 + xreal[1];
    obj[0] = xreal[0];
    obj[1] = g*(1.0  - sqrt(obj[0]/g));
    exp1 = (obj[1]-e)*cos(theta) - obj[0]*sin(theta);
    exp2 = (obj[1]-e)*sin(theta) + obj[0]*cos(theta);
    exp2 = b*PI*pow(exp2,c);
    exp2 = fabs(sin(exp2));
    exp2 = a*pow(exp2,d);
    constr[0] = exp1/exp2 - 1.0;
    return;
}


/*  Test problem CTP7
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 1
    */


void ctp7 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double g;
    double theta, a, b, c, d, e;
    double exp1, exp2;
    theta = -0.05*PI;
    a = 40.0;
    b = 5.0;
    c = 1.0;
    d = 6.0;
    e = 0.0;
    g = 1.0 + xreal[1];
    obj[0] = xreal[0];
    obj[1] = g*(1.0  - sqrt(obj[0]/g));
    exp1 = (obj[1]-e)*cos(theta) - obj[0]*sin(theta);
    exp2 = (obj[1]-e)*sin(theta) + obj[0]*cos(theta);
    exp2 = b*PI*pow(exp2,c);
    exp2 = fabs(sin(exp2));
    exp2 = a*pow(exp2,d);
    constr[0] = exp1/exp2 - 1.0;
    return;
}


/*  Test problem CTP8
    # of real variables = 2
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 2
    */


void ctp8 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double g;
    double theta, a, b, c, d, e;
    double exp1, exp2;
    g = 1.0 + xreal[1];
    obj[0] = xreal[0];
    obj[1] = g*(1.0  - sqrt(obj[0]/g));
    theta = 0.1*PI;
    a = 40.0;
    b = 0.5;
    c = 1.0;
    d = 2.0;
    e = -2.0;
    exp1 = (obj[1]-e)*cos(theta) - obj[0]*sin(theta);
    exp2 = (obj[1]-e)*sin(theta) + obj[0]*cos(theta);
    exp2 = b*PI*pow(exp2,c);
    exp2 = fabs(sin(exp2));
    exp2 = a*pow(exp2,d);
    constr[0] = exp1/exp2 - 1.0;
    theta = -0.05*PI;
    a = 40.0;
    b = 2.0;
    c = 1.0;
    d = 6.0;
    e = 0.0;
    exp1 = (obj[1]-e)*cos(theta) - obj[0]*sin(theta);
    exp2 = (obj[1]-e)*sin(theta) + obj[0]*cos(theta);
    exp2 = b*PI*pow(exp2,c);
    exp2 = fabs(sin(exp2));
    exp2 = a*pow(exp2,d);
    constr[1] = exp1/exp2 - 1.0;
    return;
}




void dtlz1 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0;
	double gx;
	int i, j;

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0) - cos(20*PI*(xreal[i]-0.5));
	}
	gx = 100 * (sum+nreal-nobj+1) + 1.0;
	sum = gx;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * xreal[j];
	}
	obj[0] = 0.5 * sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * xreal[j];
		}
		sum = sum * (1.0-xreal[nobj-1-i]);
		obj[i] = 0.5 * sum;
	}
	return;
}



void dtlz1Scaled (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0;
	double gx;
	int i, j;

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0) - cos(20*PI*(xreal[i]-0.5));
	}
	gx = 100 * (sum+nreal-nobj+1) + 1.0;
	sum = gx;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * xreal[j];
	}
	obj[0] = 0.5 * sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * xreal[j];
		}
		sum = sum * (1.0-xreal[nobj-1-i]);
		obj[i] = 0.5 * sum;
	}

	for (i=1; i<nobj; i++)
	{
		obj[i] = obj[i] * pow(10.0, i);
	}

	return;
}



void dtlz1Inverted (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0;
	double gx;
	int i, j;

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0) - cos(20*PI*(xreal[i]-0.5));
	}
	gx = 100 * (sum+nreal-nobj+1);
	sum = gx + 1.0;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * xreal[j];
	}
	obj[0] = 0.5 * sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx + 1.0;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * xreal[j];
		}
		sum = sum * (1.0-xreal[nobj-1-i]);
		obj[i] = 0.5 * sum;
	}

	for (i=0; i<nobj; i++)
	{
		obj[i] = 0.5*(1+gx) - obj[i];
	}
	return;
}



void dtlz2 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0;
	double gx;
	int i, j;

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0);
	}
	gx = 1.0 + sum;
	sum = gx;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * cos(xreal[j]*PI/2.0);
	}
	obj[0] = sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * cos(xreal[j]*PI/2.0);
		}
		sum = sum * sin(xreal[nobj-1-i]*PI/2.0);
		obj[i] = sum;
	}
	return;
}


void dtlz2Inverted  (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0;
	double gx;
	int i, j;

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0);
	}
	gx = sum;
	sum = gx + 1.0;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * cos(xreal[j]*PI/2.0);
	}
	obj[0] = sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx + 1.0;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * cos(xreal[j]*PI/2.0);
		}
		sum = sum * sin(xreal[nobj-1-i]*PI/2.0);
		obj[i] = sum;
	}
	for (i=0; i<nobj; i++)
	{
		obj[i] = (1+gx) - obj[i];
	}
	return;
}


 
void dtlz2Convex (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0;
	double gx;
	int i, j;

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0);
	}
	gx = 1.0 + sum;
	sum = gx;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * cos(xreal[j]*PI/2.0);
	}
	obj[0] = sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * cos(xreal[j]*PI/2.0);
		}
		sum = sum * sin(xreal[nobj-1-i]*PI/2.0);
		obj[i] = sum;
	}
	for (j=0; j<nobj-1; j++)
	{
		obj[j] = pow(obj[j], 4.0);
	}
	obj[nobj-1] = pow(obj[nobj-1], 2.0);

	return;
}


void dtlz2InvertedConvex  (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0;
	double gx;
	int i, j;

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0);
	}
	gx = sum;
	sum = gx + 1.0;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * cos(xreal[j]*PI/2.0);
	}
	obj[0] = sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx + 1.0;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * cos(xreal[j]*PI/2.0);
		}
		sum = sum * sin(xreal[nobj-1-i]*PI/2.0);
		obj[i] = sum;
	}

	for (i=0; i<nobj; i++)
	{
		obj[i] = (1+gx) - obj[i];
	}
	for (j=0; j<nobj-1; j++)
	{
		obj[j] = pow(obj[j], 2.0);
	}
	obj[nobj-1] = pow(obj[nobj-1], 1.0);
	
	

	return;
}



void dtlz2Scaled (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0;
	double gx;
	int i, j;

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0);
	}
	gx = 1.0 + sum;
	sum = gx;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * cos(xreal[j]*PI/2.0);
	}
	obj[0] = sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * cos(xreal[j]*PI/2.0);
		}
		sum = sum * sin(xreal[nobj-1-i]*PI/2.0);
		obj[i] = sum;
	}
	
	for (i=1; i<nobj; i++)
	{
		obj[i] = obj[i] * pow(10.0, i);
	}
	
	return;
}


void dtlz5 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{

	double sum=0;
	double gx;
	int i, j;
	double *x;
	x=(double*)malloc((nobj-1)*sizeof(double));

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0);
	}
    for (i=1; i<nobj-1; i++)
	{
		x[i] = PI/(4*(1+sum))*(1+2*sum*xreal[i]);
	}
	gx = 1.0 + sum;
	sum = gx;
	for (j=1; j<nobj-1; j++)
	{
		sum = sum * cos(x[j]);
	}
	sum = sum * cos(xreal[0]*PI/2.0);
	obj[0] = sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx;
		for (j=1; j<nobj-1-i; j++)
		{
			sum = sum * cos(x[j]);
		}
		if (i == nobj-1)
		{
			sum = sum * sin(xreal[0]*PI/2.0);
		}
		else 
		{
			sum = sum * sin(x[nobj-1-i]);
			sum = sum * cos(xreal[0]*PI/2.0);
		}		
		obj[i] = sum;
	}
	free (x);
	return;
}
 
void dtlz5I (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	
	double sum=0;
	double gx;
	int i, j;
	double *x;
	x=(double*)malloc((nobj-1)*sizeof(double));

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0);
	}
	
	for (i=0; i<I_number-1; i++)
	{
		x[i] = xreal[i]*PI/2.0;
	}
    for (i=I_number-1; i<nobj-1; i++)
	{
		x[i] = PI/(4*(1+sum))*(1+2*sum*xreal[i]);
	}
	gx = 1.0 + 100 * sum;
	
	sum = gx;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * cos(x[j]);
	}
	obj[0] = sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * cos(x[j]);
		}
		sum = sum * sin(x[nobj-1-i]);
		obj[i] = sum;
	}
	free (x);
	return;
}




void dtlz6 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	
	double sum=0;
	double gx;
	int i, j;
	double *x;
	x=(double*)malloc((nobj-1)*sizeof(double));

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]), 0.1);
	}
    for (i=1; i<nobj-1; i++)
	{
		x[i] = PI/(4*(1+sum))*(1+2*sum*xreal[i]);
	}
	gx = 1.0 + sum;
	sum = gx;
	for (j=1; j<nobj-1; j++)
	{
		sum = sum * cos(x[j]);
	}
	sum = sum * cos(xreal[0]*PI/2.0);
	obj[0] = sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx;
		for (j=1; j<nobj-1-i; j++)
		{
			sum = sum * cos(x[j]);
		}
		if (i == nobj-1)
		{
			sum = sum * sin(xreal[0]*PI/2.0);
		}
		else 
		{
			sum = sum * sin(x[nobj-1-i]);
			sum = sum * cos(xreal[0]*PI/2.0);
		}		
		obj[i] = sum;
	}
	free (x);
	return;
}




void dtlz7 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0, temp=0;
	double gx;
	int i, j;

	for (i=nobj-1; i<nreal; i++)
	{
		sum += xreal[i];
	}
	gx = 1.0 + 9.0*sum/(nreal-nobj+1.0);
	sum = gx;
    for (i=0; i<nobj-1; i++)
	{
		obj[i] = xreal[i];
	}
	for (i=0; i<nobj-1; i++)
	{
		temp += (obj[i]/(sum+1))*(1+sin(3*PI*obj[i]));
	}
	temp = nobj-temp;
	obj[nobj-1] = (sum+1)*temp;
	return;
}


void dtlz3 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0;
	double gx;
	int i, j;

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0) - cos(20*PI*(xreal[i]-0.5));
	}
	gx = 100 * (sum+nreal-nobj+1) + 1.0;
	sum = gx;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * cos(xreal[j]*PI/2.0);
	}
	obj[0] = sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * cos(xreal[j]*PI/2.0);
		}
		sum = sum * sin(xreal[nobj-1-i]*PI/2.0);
		obj[i] = sum;
	}
	return;

}

void dtlz3Convex (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0;
	double gx;
	int i, j;

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0) - cos(20*PI*(xreal[i]-0.5));
	}
	gx = 100 * (sum+nreal-nobj+1) + 1.0;
	sum = gx;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * cos(xreal[j]*PI/2.0);
	}
	obj[0] = sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * cos(xreal[j]*PI/2.0);
		}
		sum = sum * sin(xreal[nobj-1-i]*PI/2.0);
		obj[i] = sum;
	}

	for (j=0; j<nobj-1; j++)
	{
		obj[j] = pow(obj[j], 4.0);
	}
	obj[nobj-1] = pow(obj[nobj-1], 2.0);

	return;

}


	



void dtlz4 (double *_xreal, double *xbin, int **gene, double *obj, double *constr)
{
	double sum=0;
	double gx;
	int i, j;
	double *xreal;
	xreal = (double *)malloc(nreal * sizeof(double));
	memcpy(xreal, _xreal, nreal * sizeof(double));

	for (i=nobj-1; i<nreal; i++)
	{
		sum += pow ((xreal[i]-0.5), 2.0);
	}
	for (i=0; i<nobj-1; i++)
	{
		xreal[i]=pow((float)xreal[i],(float)100);
	}
	gx = 1.0 + sum;
	sum = gx;
	for (j=0; j<nobj-1; j++)
	{
		sum = sum * cos(xreal[j]*PI/2.0);
	}
	obj[0] = sum;

	for (i=1; i<nobj; i++)
	{
		sum = gx;
		for (j=0; j<nobj-1-i; j++)
		{
			sum = sum * cos(xreal[j]*PI/2.0);
		}
		sum = sum * sin(xreal[nobj-1-i]*PI/2.0);
		obj[i] = sum;
	}
	free(xreal);
	return;
}



void lzdt1 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = xreal[0];
    g = 0.0;
    for (i=1; i<nreal; i++)
    {
        g += pow((xreal[i]-xreal[0]),2.0);
    }
    g = 9.0*g/(nreal-1);
    g += 1.0;
    h = 1.0 - sqrt(f1/g);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}


void lzdt2 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = xreal[0];
    g = 0.0;
    for (i=1; i<nreal; i++)
    {
        g += pow((xreal[i]-xreal[0]),2.0);
    }
    g = 9.0*g/(nreal-1);
    g += 1.0;
    h = 1.0 - pow((f1/g),2.0);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}



void lzdt3 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = xreal[0];
    g = 0.0;
    for (i=1; i<nreal; i++)
    {
        g += pow((xreal[i]-xreal[0]),2.0);
    }
    g = 9.0*g/(nreal-1);
    g += 1.0;
    h = 1.0 - sqrt(f1/g) - (f1/g)*sin(10.0*PI*f1);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}



void lzdt6 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = 1.0 - (exp(-4.0*xreal[0]))*pow((sin(4.0*PI*xreal[0])),6.0);
    g = 0.0;
    for (i=1; i<10; i++)
    {
        g += pow((xreal[i]-xreal[0]),2.0);
    }
    g = g/9.0;
    g = pow(g,0.25);
    g = 1.0 + 9.0*g;
    h = 1.0 - pow((f1/g),2.0);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}



void qzdt1 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = xreal[0];
    g = 0.0;
    for (i=1; i<nreal; i++)
    {
        g += pow((pow(xreal[i],2.0)-xreal[0]),2.0);
    }
    g = 9.0*g/(nreal-1);
    g += 1.0;
    h = 1.0 - sqrt(f1/g);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}



void qzdt2 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = xreal[0];
    g = 0.0;
    for (i=1; i<nreal; i++)
    {
        g += pow((pow(xreal[i],2.0)-xreal[0]),2.0);
    }
    g = 9.0*g/(nreal-1);
    g += 1.0;
    h = 1.0 - pow((f1/g),2.0);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}



void qzdt3 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = xreal[0];
    g = 0.0;
    for (i=1; i<nreal; i++)
    {
        g += pow((pow(xreal[i],2.0)-xreal[0]),2.0);
    }
    g = 9.0*g/(nreal-1);
    g += 1.0;
    h = 1.0 - sqrt(f1/g) - (f1/g)*sin(10.0*PI*f1);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}



void qzdt6 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
    double f1, f2, g, h;
    int i;
    f1 = 1.0 - (exp(-4.0*xreal[0]))*pow((sin(4.0*PI*xreal[0])),6.0);
    g = 0.0;
    for (i=1; i<10; i++)
    {
        g += pow((pow(xreal[i],2.0)-xreal[0]),2.0);
    }
    g = g/9.0;
    g = pow(g,0.25);
    g = 1.0 + 9.0*g;
    h = 1.0 - pow((f1/g),2.0);
    f2 = g*h;
    obj[0] = f1;
    obj[1] = f2;
    return;
}




void ldtlz2 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	
	double sum=0;
	double gx;
	int i;
	for(i=2;i<nreal;i++)
		sum+=pow((xreal[i]-xreal[0]),2.0);
	gx=1.0+sum;
    obj[0]=gx*cos(xreal[0]*PI/2.0)*cos(xreal[1]*PI/2.0);
    obj[1]=gx*cos(xreal[0]*PI/2.0)*sin(xreal[1]*PI/2.0);
	obj[2]=gx*sin(xreal[0]*PI/2.0);
	
}



void qdtlz2 (double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	
	double sum=0;
	double gx;
	int i;
	for(i=2;i<nreal;i++)
		sum+=pow((pow(xreal[i],2.0)-xreal[0]),2.0);
	gx=1.0+sum;
    obj[0]=gx*cos(xreal[0]*PI/2.0)*cos(xreal[1]*PI/2.0);
    obj[1]=gx*cos(xreal[0]*PI/2.0)*sin(xreal[1]*PI/2.0);
	obj[2]=gx*sin(xreal[0]*PI/2.0);
	
}


/*  Test problem UF1
    # of real variables = 30(in CEC09)
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */

void uf1(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	unsigned int j, count1, count2;
	double sum1, sum2, yj;
	
	sum1   = sum2   = 0.0;
	count1 = count2 = 0;
	for(j = 2; j <= nreal; j++) 
	{
		yj = xreal[j-1] - sin(6.0*PI*xreal[0] + j*PI/nreal);
		yj = yj * yj;
		if(j % 2 == 0) 
		{
			sum2 += yj;
			count2++;
		} 
		else 
		{
			sum1 += yj;
			count1++;
		}
	}
	obj[0] = xreal[0]				+ 2.0 * sum1 / (double)count1;
	obj[1] = 1.0 - sqrt(xreal[0]) + 2.0 * sum2 / (double)count2;	
}


/*  Test problem UF2
    # of real variables = 30(in CEC09)
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */

void uf2(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	unsigned int j, count1, count2;
	double sum1, sum2, yj;
	
	sum1   = sum2   = 0.0;
	count1 = count2 = 0;
	for(j = 2; j <= nreal; j++) 
	{
		if(j % 2 == 0) 
		{
			yj = xreal[j-1]-0.3*xreal[0]*(xreal[0]*cos(24.0*PI*xreal[0]+4.0*j*PI/nreal)+2.0)*sin(6.0*PI*xreal[0]+j*PI/nreal);
			sum2 += yj*yj;
			count2++;
		} 
		else 
		{
			yj = xreal[j-1]-0.3*xreal[0]*(xreal[0]*cos(24.0*PI*xreal[0]+4.0*j*PI/nreal)+2.0)*cos(6.0*PI*xreal[0]+j*PI/nreal);
			sum1 += yj*yj;
			count1++;
		}
	}
	obj[0] = xreal[0]			  + 2.0 * sum1 / (double)count1;
	obj[1] = 1.0 - sqrt(xreal[0]) + 2.0 * sum2 / (double)count2;	
}


/*  Test problem UF3
    # of real variables = 30(in CEC09)
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */

void uf3(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	unsigned int j, count1, count2;
	double sum1, sum2, prod1, prod2, yj, pj;
	
	sum1   = sum2   = 0.0;
	count1 = count2 = 0;
	prod1  = prod2  = 1.0;
	for(j = 2; j <= nreal; j++) 
	{
		yj = xreal[j-1]-pow(xreal[0],0.5*(1.0+3.0*(j-2.0)/(nreal-2.0)));
		pj = cos(20.0*yj*PI/sqrt(j+0.0));
		if (j % 2 == 0) 
		{
			sum2  += yj*yj;
			prod2 *= pj;
			count2++;
		} 
		else 
		{
			sum1  += yj*yj;
			prod1 *= pj;
			count1++;
		}
	}
	obj[0] = xreal[0]				+ 2.0*(4.0*sum1 - 2.0*prod1 + 2.0) / (double)count1;
	obj[1] = 1.0 - sqrt(xreal[0]) + 2.0*(4.0*sum2 - 2.0*prod2 + 2.0) / (double)count2;
}


/*  Test problem UF4
    # of real variables = 30(in CEC09)
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */

void uf4(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	unsigned int j, count1, count2;
	double sum1, sum2, yj, hj;
	
	sum1   = sum2   = 0.0;
	count1 = count2 = 0;
	for(j = 2; j <= nreal; j++) 
	{
		yj = xreal[j-1]-sin(6.0*PI*xreal[0]+j*PI/nreal);
		hj = fabs(yj)/(1.0+exp(2.0*fabs(yj)));
		if (j % 2 == 0) 
		{
			sum2  += hj;
			count2++;
		} 
		else 
		{
			sum1  += hj;
			count1++;
		}
	}
	obj[0] = xreal[0]				+ 2.0*sum1 / (double)count1;
	obj[1] = 1.0 - xreal[0]*xreal[0]	+ 2.0*sum2 / (double)count2;
}


/*  Test problem UF5
    # of real variables = 30(in CEC09)
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */

void uf5(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	unsigned int j, count1, count2;
	double sum1, sum2, yj, hj,Nm, Em;
	
	sum1   = sum2   = 0.0;
	count1 = count2 = 0;
	Nm = 10.0; Em = 0.1;
	for(j = 2; j <= nreal; j++) 
	{
		yj = xreal[j-1]-sin(6.0*PI*xreal[0]+j*PI/nreal);
		hj = 2.0*yj*yj - cos(4.0*PI*yj) + 1.0;
		if (j % 2 == 0) 
		{
			sum2  += hj;
			count2++;
		} 
		else 
		{
			sum1  += hj;
			count1++;
		}
	}
	hj = (0.5/Nm + Em)*fabs(sin(2.0*Nm*PI*xreal[0]));
	obj[0] = xreal[0]	      + hj + 2.0*sum1 / (double)count1;
	obj[1] = 1.0 - xreal[0] + hj + 2.0*sum2 / (double)count2;
}


/*  Test problem UF6
    # of real variables = 30(in CEC09)
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */

void uf6(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	unsigned int j, count1, count2;
	double sum1, sum2, prod1, prod2, yj, hj, pj, Nm, Em;
	Nm = 2.0; Em = 0.1;
	
	sum1   = sum2   = 0.0;
	count1 = count2 = 0;
	prod1  = prod2  = 1.0;
	for(j = 2; j <= nreal; j++) 
	{
		yj = xreal[j-1]-sin(6.0*PI*xreal[0]+j*PI/nreal);
		pj = cos(20.0*yj*PI/sqrt(j+0.0));
		if (j % 2 == 0) 
		{
			sum2  += yj*yj;
			prod2 *= pj;
			count2++;
		} 
		else 
		{
			sum1  += yj*yj;
			prod1 *= pj;
			count1++;
		}
	}
	
	hj = 2.0*(0.5/Nm + Em)*sin(2.0*Nm*PI*xreal[0]);
	if(hj<0.0) hj = 0.0;
	obj[0] = xreal[0]	      + hj + 2.0*(4.0*sum1 - 2.0*prod1 + 2.0) / (double)count1;
	obj[1] = 1.0 - xreal[0] + hj + 2.0*(4.0*sum2 - 2.0*prod2 + 2.0) / (double)count2;
}


/*  Test problem UF7
    # of real variables = 30(in CEC09)
    # of bin variables = 0
    # of objectives = 2
    # of constraints = 0
    */

void uf7(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	unsigned int j, count1, count2;
	double sum1, sum2, yj;
	
	sum1   = sum2   = 0.0;
	count1 = count2 = 0;
	for(j = 2; j <= nreal; j++) 
	{
		yj = xreal[j-1] - sin(6.0*PI*xreal[0]+j*PI/nreal);
		if (j % 2 == 0) 
		{
			sum2  += yj*yj;
			count2++;
		} 
		else 
		{
			sum1  += yj*yj;
			count1++;
		}
	}
	yj = pow(xreal[0],0.2);
	obj[0] = yj	    + 2.0*sum1 / (double)count1;
	obj[1] = 1.0 - yj + 2.0*sum2 / (double)count2;
}


/*  Test problem UF8
    # of real variables = 30(in CEC09)
    # of bin variables = 0
    # of objectives = 3
    # of constraints = 0
    */

void uf8(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{	
	unsigned int j, count1, count2, count3;
	double sum1, sum2, sum3, yj;
	
	sum1   = sum2   = sum3   = 0.0;
	count1 = count2 = count3 = 0;
	for(j = 3; j <= nreal; j++) 
	{
		yj = xreal[j-1] - 2.0*xreal[1]*sin(2.0*PI*xreal[0]+j*PI/nreal);
		if(j % 3 == 1) 
		{
			sum1  += yj*yj;
			count1++;
		} 
		else if(j % 3 == 2) 
		{
			sum2  += yj*yj;
			count2++;
		}
		else
		{
			sum3  += yj*yj;
			count3++;
		}
	}
	obj[0] = cos(0.5*PI*xreal[0])*cos(0.5*PI*xreal[1]) + 2.0*sum1 / (double)count1;
	obj[1] = cos(0.5*PI*xreal[0])*sin(0.5*PI*xreal[1]) + 2.0*sum2 / (double)count2;
	obj[2] = sin(0.5*PI*xreal[0])                  + 2.0*sum3 / (double)count3;
	
}


/*  Test problem UF9
    # of real variables = 30(in CEC09)
    # of bin variables = 0
    # of objectives = 3
    # of constraints = 0
    */

void uf9(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{	
	unsigned int j, count1, count2, count3;
	double sum1, sum2, sum3, yj, Em;
	
	Em = 0.1;
	sum1   = sum2   = sum3   = 0.0;
	count1 = count2 = count3 = 0;
	for(j = 3; j <= nreal; j++) 
	{
		yj = xreal[j-1] - 2.0*xreal[1]*sin(2.0*PI*xreal[0]+j*PI/nreal);
		if(j % 3 == 1) 
		{
			sum1  += yj*yj;
			count1++;
		} 
		else if(j % 3 == 2) 
		{
			sum2  += yj*yj;
			count2++;
		}
		else
		{
			sum3  += yj*yj;
			count3++;
		}
	}
	yj = (1.0+Em)*(1.0-4.0*(2.0*xreal[0]-1.0)*(2.0*xreal[0]-1.0));
	if(yj<0.0) yj = 0.0;
	obj[0] = 0.5*(yj + 2*xreal[0])*xreal[1]		+ 2.0*sum1 / (double)count1;
	obj[1] = 0.5*(yj - 2*xreal[0] + 2.0)*xreal[1] + 2.0*sum2 / (double)count2;
	obj[2] = 1.0 - xreal[1]                   + 2.0*sum3 / (double)count3;
}


/*  Test problem UF10
    # of real variables = 30(in CEC09)
    # of bin variables = 0
    # of objectives = 3
    # of constraints = 0
    */

void uf10(double *xreal, double *xbin, int **gene, double *obj, double *constr)
{
	
	unsigned int j, count1, count2, count3;
	double sum1, sum2, sum3, yj, hj;
	
	sum1   = sum2   = sum3   = 0.0;
	count1 = count2 = count3 = 0;
	for(j = 3; j <= nreal; j++) 
	{
		yj = xreal[j-1] - 2.0*xreal[1]*sin(2.0*PI*xreal[0]+j*PI/nreal);
		hj = 4.0*yj*yj - cos(8.0*PI*yj) + 1.0;
		if(j % 3 == 1) 
		{
			sum1  += hj;
			count1++;
		} 
		else if(j % 3 == 2) 
		{
			sum2  += hj;
			count2++;
		}
		else
		{
			sum3  += hj;
			count3++;
		}
	}
	obj[0] = cos(0.5*PI*xreal[0])*cos(0.5*PI*xreal[1]) + 2.0*sum1 / (double)count1;
	obj[1] = cos(0.5*PI*xreal[0])*sin(0.5*PI*xreal[1]) + 2.0*sum2 / (double)count2;
	obj[2] = sin(0.5*PI*xreal[0])                      + 2.0*sum3 / (double)count3;
}




