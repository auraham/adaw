/*==========================================================================
//  Implementation of AdaW
//  Last update 16, July, 2019
//
//  The source code was implemented by Miqing Li (http://www.cs.bham.ac.uk/~limx).  
//
//  The codes are free for reserach work.
//  If you have any problem with the source codes, please contact 
//  Miqing Li at limitsing@gmail.com
===========================================================================*/

// NOTE: This code is for multiobjective minimization problems; for maximization problems, some procedures may need to be modified. 

# include <string>
# include <sstream>
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <time.h>

# include "Global.h" 
# include "Random.h"
# include "Problem.h"

int main (int argc, char **argv)
{
    int i, j, k;
	char *name;
	int nrun = 1;
	int random;
	double start, end;

	population *pop;				// population
	weightVectors *weight;			// weight
	ind_list *archive;				// archive

	int h;							// specification of the number of weights 
	int niche;						// niche size
	
	double update_frequency = 0.05;		// weight update frequency 
	double freeze_time = 0.9;			// the time (ratio) of the evolution when weights are not allowed to change


	srand((unsigned)time(NULL));
    random = rand()%1000;
    seed=(float) random/1000.0;
    
    // debug
    seed = 0.5;
    printf("DEBUG: Using fixed seed: %.4f\n", seed);
    
    if (seed<=0.0 || seed>=1.0)
    {
        printf("\n Entered seed value is wrong, seed value must be in (0,1) \n");
        exit(1);
	}


	char *names[] = {"dtlz1_3", "dtlz2_3", "dtlz2Convex_3", 
		"dtlz1Inverted_3", "dtlz2Inverted_3",
		"sch1", "fon1",
		"sch2", "zdt3", "dtlz7_3",
		"dtlz5_3", "vnt2",
		"dtlz1Scaled_3", "dtlz2Scaled_3",
		"dtlz1Inverted_10", "dtlz2_10", "dtlz5I_10"};

	const char *paths[] = {"Output/dtlz1/3", "Output/dtlz2/3", "Output/dtlz2Convex/3",
		"Output/dtlz1Inverted/3", "Output/dtlz2Inverted/3",
		"Output/sch1", "Output/fon1",
		"Output/sch2", "Output/zdt3", "Output/dtlz7/3",
		"Output/dtlz5/3", "Output/vnt2",
		"Output/dtlz1Scaled/3", "Output/dtlz2Scaled/3",
		"Output/dtlz1Inverted/10", "Output/dtlz2/10", "Output/dtlz5I/2"};


    typedef void _TTestProblem(double *, double *, int **, double *, double *);
    _TTestProblem *problems[] = {dtlz1, dtlz2, dtlz2Convex, 
		dtlz1Inverted, dtlz2Inverted,
		sch1, fon1,
		sch2, zdt3, dtlz7,
		dtlz5, vnt2,
		dtlz1Scaled, dtlz2Scaled,
		dtlz1Inverted, dtlz2, dtlz5I};


	typedef void _TSpace(double *, double *);
	_TSpace *spaces[] = {dtlz1_space, dtlz2_space, dtlz2Convex_space, 
		dtlz1Inverted_space, dtlz2Inverted_space,
		sch1_space, fon1_space,
		sch2_space, zdt3_space, dtlz7_space,
		dtlz5_space, vnt2_space,
		dtlz1Scaled_space, dtlz2Scaled_space,
		dtlz1Inverted_space, dtlz2_space, dtlz5I_space};

    int objectives[] = {3, 3, 3, 
		3, 3,
		2, 2,
		2, 2, 3,
		3, 3,
		3, 3,
		10, 10, 10};

    int decisions[] = {7, 12, 12, 
		7, 12,
		1, 2,
		1, 30, 22,
		12, 2,
		7, 12,
		14, 19, 19};


	int constraints[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    int evaluations[] = {30000, 30000, 30000, 
		30000, 30000,
		25000, 25000,
		25000, 25000, 30000,
		30000, 30000,
		30000, 30000, 
		100000, 100000, 100000};
	int hs[] = {13, 13, 13, 
		13, 13,
		99, 99,
		99, 99, 13,
		13, 13,
		13, 13,
		3, 3, 3};

    for (int n = 0; n < sizeof(problems) / sizeof(*problems); ++n)
    {
		name = names[n];
		neval = evaluations[n];
		const std::string path = paths[n];
    	test_problem = problems[n];
    	nreal = decisions[n];
		ncon= constraints[n];
		nobj = objectives[n];
		h = hs[n];

		randomize();
		
/*-------initialize weights------start---------*/
		weight_size = combination(h+nobj-1, nobj-1);			// calculate the number of weight vectors (i.e., population size)
		double **initial_weights;
		initial_weights = (double **)malloc(weight_size*sizeof(double*));
		for(i=0; i<weight_size; i++)
		{
			initial_weights[i] = (double *)malloc(nobj*sizeof(double));
		}
		int *temp_weight;
		temp_weight = (int *)malloc(nobj*sizeof(int));
		weight_size = 0;
		init_weight(0, h, initial_weights, 0, temp_weight);
		free(temp_weight);
		permutation(initial_weights);
/*-------initialize weights------end---------*/

		popsize = weight_size;									// population size
/* Note: although the presetting population size is the number of weight vectors, 
actual population size may be smaller than the number of weight vectors 
since there may exist several weight vectors associated with one individual. 
This is like the case in generic decomposition-based algorithms that several weight vectors have duplicate individuals. */


		archive_capacity = 2*weight_size;						// capacity of the archive set
		
		pop = (population *)malloc(sizeof(population));
		allocate_memory_pop (pop, popsize+archive_capacity);

		weight = (weightVectors *)malloc((2*weight_size+archive_capacity)*sizeof(weightVectors));	
		// a larger set of the weights are allocated for ease of calculations		
		for (i=0; i<2*weight_size+archive_capacity; i++)
		{
			allocate_memory_weight (&weight[i]);
			weight[i].active = 0;
		}
		for(i=0; i<weight_size; i++)
		{
			for(j=0; j<nobj; j++)
			{
				weight[i].weight_vector[j] = initial_weights[i][j];
			}
		}

		archive_size = 0;						// the number of current individuals in the archive set
		archive = (ind_list *)malloc(sizeof(ind_list));
		archive->ind = (individual *)malloc(sizeof(individual));
		allocate_memory_ind (archive->ind);
		archive->parent = NULL;
		archive->child = NULL;

//  parameters in the basic MOEA/D
		niche = popsize / 10;
		prob = 0.9;								// selection probability from the whole population or neighborhood
		nr = popsize / 100;						// maximum number of the replacement times


		ideal_point = (double *)malloc(nobj*sizeof(double));
		min_realvar = (double *)malloc(nreal*sizeof(double));
        max_realvar = (double *)malloc(nreal*sizeof(double));
		min_pf = (double *)malloc(nobj*sizeof(double));
		max_pf = (double *)malloc(nobj*sizeof(double));	
		spaces[n](min_realvar, max_realvar);

		initialize_neighbourhood(weight, niche);		// initialise the neighbourhood

		FILE *fptObj = fopen((path + "/" + "obj.txt").c_str(), "w");
		FILE *fptVar = fopen((path + "/" + "var.txt").c_str(), "w");


		pcross_real=1.0;
		pmut_real=1.0/nreal;
		eta_c=20.0;
		eta_m=20.0;

/* I_number (nobj>=I_number>= 2) is the dimension of the Pareto front in DTLZ5(I,M), set by users */
		I_number = 2;           
		
		for (j=0; j<nrun; j++)
		{
			printf("\n %s; %d run; 0 gen......", name, j);
			currenteval = 0;
// initialize reference point
			for (k=0; k<nobj; k++)
			{
				ideal_point[k] = INF;
			}
// restore information (e.g., weights and individuals) after every execution
			permutation(initial_weights);
			for(i=0; i<weight_size; i++)
			{
				for(k=0; k<nobj; k++)
				{
					weight[i].weight_vector[k] = initial_weights[i][k];
				}
			}
			popsize = weight_size;
			initialization (pop, archive);
			initialize_match (weight, pop);	
			initialize_neighbourhood(weight, niche);
			
			printf("\n Initialization done, now performing first generation"); 	

			start = clock();
			i = 1;
			while (currenteval<neval)
			{
				printf("\n %s; %d run; %d gen......", name, j, i);
// evolution procedure of a decomposition-based algorithm
				variation_update (weight, pop, niche, archive);			

				if (archive_size > archive_capacity)
				{
// maintenance operation in the archive set
					maintain_archive(archive);		
				}

				if (i % int(update_frequency*(neval/weight_size)) == 0 && currenteval < freeze_time*neval)  // neval/weight_size = total generations
				{
					if (add_weights(archive, pop, weight, niche) > 0)	// add weight vectors into the population   
					{
// remove weight vectors
						remove_weights(weight, pop);	
					}
// exchange individuals to make sure that the front individuals are active (i.e., correspond to at least one weight vector)
					popsize = reorganize_pop(weight, pop, popsize);	
// update neighboring weight vectors of each active weight vector
					update_neighbourhood(weight, niche, 2*weight_size+archive_capacity);	
					printf("\tweight update");
				}								 
				i ++;
			}
	
			printf("\n Generations finished, now reporting solutions");

			end = clock();
			printf("\n time is %f\n",(end-start)/CLOCKS_PER_SEC);

			report_obj(pop, fptObj);
			fprintf(fptObj,"\n");
			fflush(stdout);
			fflush(fptObj);
			
			report_variable(pop,fptVar);
			fprintf(fptVar,"\n");
			fflush(stdout);
			fflush(fptVar);

			clear_pop(pop);
			clear_weight(weight, 2*weight_size+archive_capacity);
			clear_archive(archive);
		}

		popsize = weight_size;
		deallocate_memory_pop (pop, popsize+archive_capacity);
		free (pop);
		free (ideal_point);

		for (i=0; i<2*weight_size+archive_capacity; i++)
		{
			deallocate_memory_weight (&weight[i]);
		}
		free(weight);
		
        free (min_realvar);
        free (max_realvar);
		free (min_pf);
		free (max_pf);

		for(i=0; i<weight_size; i++)
		{
			free(initial_weights[i]);
		}
		free(initial_weights);

		fclose(fptObj);
		fclose(fptVar);
    }

    printf("\n Routine successfully exited \a\n");
	
    return (0);
}
