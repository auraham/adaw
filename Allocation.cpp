# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Function to allocate memory to a population */
void allocate_memory_pop (population *pop, int size)
{
    int i;
    pop->ind = (individual *)malloc(size*sizeof(individual));
    for (i=0; i<size; i++)
    {
        allocate_memory_ind (&(pop->ind[i]));
    }
    return;
}


/* Function to allocate memory to an individual */
void allocate_memory_ind (individual *ind)
{
    int j;
    if (nreal != 0)
    {
        ind->xreal = (double *)malloc(nreal*sizeof(double));
    }
    if (nbin != 0)
    {
        ind->xbin = (double *)malloc(nbin*sizeof(double));
        ind->gene = (int **)malloc(nbin*sizeof(int));
        for (j=0; j<nbin; j++)
        {
            ind->gene[j] = (int *)malloc(nbits[j]*sizeof(int));
        }
    }
    ind->obj = (double *)malloc(nobj*sizeof(double));
	ind->obj_norm = (double *)malloc(nobj*sizeof(double));
	
	ind->best_weight = -1;
	ind->nweight = 0;
	
	ind->weight_index = (list *)malloc(sizeof(list));
	ind->weight_index->index = -1;
	ind->weight_index->parent = NULL;
	ind->weight_index->child = NULL;

	ind->niche_neighbor = (list *)malloc(sizeof(list));
	ind->niche_neighbor->index = -1;
	ind->niche_neighbor->parent = NULL;
	ind->niche_neighbor->child = NULL;

    if (ncon != 0)
    {
        ind->constr = (double *)malloc(ncon*sizeof(double));
    }
    return;
}


/* Function to deallocate memory to a population */
void deallocate_memory_pop (population *pop, int size)
{
    int i;
    for (i=0; i<size; i++)
    {
        deallocate_memory_ind (&(pop->ind[i]));
    }
    free (pop->ind);
    return;
}

/* Function to deallocate memory to an individual */
void deallocate_memory_ind (individual *ind)
{
    int j;
	list *temp;

    if (nreal != 0)
    {
        free(ind->xreal);
    }
    if (nbin != 0)
    {
        for (j=0; j<nbin; j++)
        {
            free(ind->gene[j]);
        }
        free(ind->xbin);
        free(ind->gene);
    }
    free(ind->obj);
	free(ind->obj_norm);

	temp = ind->weight_index->child;
    while (temp!=NULL)
    {
        temp = del(temp);
        temp = temp->child;
    }
	free(ind->weight_index);

	temp = ind->niche_neighbor->child;
	while (temp != NULL)
    {
        temp = del(temp);
        temp = temp->child;
    }
	free(ind->niche_neighbor);

    if (ncon != 0)
    {
        free(ind->constr);
    }
    return;
}

/* Function to allocate memory to a weight vector */
void allocate_memory_weight (weightVectors *weight)
{
	weight->weight_vector = (double *)malloc(nobj*sizeof(double));
	weight->neighbor = (int *)malloc(weight_size*sizeof(int));
	weight->fTch = (double *)malloc(nobj*sizeof(double));
    return;
}

/* Function to deallocate memory to a weight vector */
void deallocate_memory_weight (weightVectors *weight)
{
	free(weight->weight_vector);
	free(weight->fTch);
	free(weight->neighbor);
	return;
}
