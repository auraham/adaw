# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "Global.h" 
# include "Random.h"

/* Routine to initialize a set of evely-distributed weight vectors */
void init_weight(int index_obj, int h, double **all_weights, int pre_sum, int* temp_weight)
{
	int i, j;

	if (index_obj != nobj - 2)
	{
		for (i=0; i <= h - pre_sum; i++)
		{
			temp_weight[index_obj] = i;
			init_weight(index_obj+1, h, all_weights, pre_sum+i, temp_weight);
		}
	}
	else
	{
		for (i=0; i <= h - pre_sum; i++)
		{
			temp_weight[index_obj] = i;
			temp_weight[index_obj+1] = h - (pre_sum + i);
			for (j=0; j<nobj; j++)
			{
				all_weights[weight_size][j] = (double)temp_weight[j] / (double)h;
			}
			weight_size ++;
		}
	}
	return; 
}

/* Routine for permutating weight vectors */
void permutation(double **totalweight)
{
	int i, j; 
	int rand;
	double temp;

	for (i=0; i<weight_size; i++)
    {
        rand = rnd (i, weight_size-1);
        for(j=0; j<nobj; j++)
		{
			temp = totalweight[rand][j];
			totalweight[rand][j] = totalweight[i][j];
			totalweight[i][j] = temp;
		}
	}
	return;
}

/* Routine for match initialization between individuals and weight vectors */
void initialize_match (weightVectors *weight, population *pop)
{
	int i;
	for (i=0; i<weight_size; i++)
	{
		associate_weightWithInd(weight, i, &pop->ind[i], i);
	}
	
	return;
}

/* Routine to associate a weight vector with an individual */
void associate_weightWithInd(weightVectors *weight, int weightIndex, individual *ind, int indIndex)
{
	list *temp;

// associate the weight vector with the individual
	weight[weightIndex].active = 1;
	weight[weightIndex].fvalue = Tchebycheff(ind->obj, weight[weightIndex].weight_vector);
	weight[weightIndex].ind_index = indIndex;

// update the individual 
	if (ind->nweight == 0)
	{
		ind->nweight = 1;
		ind->best_weight = weightIndex;
		insert(ind->weight_index, weightIndex);
	}
	else
	{
		ind->nweight ++;
		if (weight[weightIndex].fvalue < weight[ind->best_weight].fvalue)
		{
			ind->best_weight = weightIndex;
		}
		temp = ind->weight_index;
		while (temp->child != NULL && weight[weightIndex].fvalue < weight[temp->child->index].fvalue)
		{
			temp = temp->child;
		}
		insert(temp, weightIndex);
	}

	return;
}

/* Routine to remove a weight vector asscoated with an individual */
void remove_weightInInd(individual *ind, int indIndex, weightVectors *weight, int weightIndex)
{
	list *temp;
	double min;
	int index;

	if (ind->nweight == 0)
	{
		printf("\n something wrong \n");
        exit(1);
	}
	if (ind->nweight == 1)
	{
		ind->nweight = 0;
		ind->best_weight = -1;
		temp = ind->weight_index->child;
		temp = del(temp);
	}
	else
	{
		ind->nweight --;
		temp = ind->weight_index->child;
		while (temp != NULL)
		{
			if (temp->index == weightIndex)
			{
				break;
			}
			temp = temp->child;
		}
		temp = del(temp);

		if (ind->best_weight == weightIndex)
		{
			temp = ind->weight_index->child;
			min = weight[temp->index].fvalue;
			index = temp->index;
			temp = temp->child;
			while (temp != NULL)
			{
				if (weight[temp->index].fvalue < min)
				{
					min = weight[temp->index].fvalue;
					index = temp->index;
				}
				temp = temp->child;
			}
			ind->best_weight = index;
		}
	}
	return;
}


/* Routine to update the match of weight vectors to an individual */
void update_match(weightVectors *weight, int weightIndex, population *pop, int oldID, int newID)
{
// remove the weight vector in the individual that the weight vector corresponds to before
	remove_weightInInd(&pop->ind[oldID], oldID, weight, weightIndex);
	
// associate the weight vector with the individual that it corresponds to now
	associate_weightWithInd(weight, weightIndex, &pop->ind[newID], newID);

	return;
}


/* Routine to exchange the position of an individual */
void exchange_ind(weightVectors *weight, population *pop, int oldID, int newID)
{
	list *temp, *temp2;

// update the information of new individual
	copy_ind(&pop->ind[oldID], &pop->ind[newID]);

	pop->ind[newID].best_weight = pop->ind[oldID].best_weight;
	pop->ind[newID].nweight = pop->ind[oldID].nweight;
	temp2 = pop->ind[newID].weight_index;
	temp = pop->ind[oldID].weight_index->child;
	while (temp != NULL)
	{
		insert(temp2, temp->index);
		temp = temp->child;
		temp2 = temp2->child;
	}

// update the information of the correponding weight vector
	temp = pop->ind[oldID].weight_index->child;
	while (temp != NULL)
	{
		weight[temp->index].ind_index = newID;
		temp = temp->child;
	}

// update the information of old individual
	pop->ind[oldID].best_weight = -1;
	pop->ind[oldID].nweight = 0;

	temp = pop->ind[oldID].weight_index->child;
    while (temp!=NULL)
    {
        temp = del(temp);
        temp = temp->child;
    }

	return;
}

/* Routine to determine the optimal weight vector of an individual */
void determine_weight(weightVectors *weight, individual *ind)
{
	int j;
	double *f_ideal;
	double sum;

	f_ideal = (double*)malloc(nobj*sizeof(double));
	
	sum = 0;
	for(j=0; j<nobj; j++)
	{
		f_ideal[j] = fabs(ind->obj[j] - ideal_point[j]);
		sum += f_ideal[j];
	}


	for(j=0; j<nobj; j++)
	{
		weight->weight_vector[j] = f_ideal[j]/sum;
	}
	
	free(f_ideal);
	return;
}

/* Routine to a weight vector in the population */
void remove_weight(weightVectors *weight, individual *ind)
{
	list *temp;

	weight->active = 0;
	weight->fvalue = INF;
	weight->ind_index = -1;
	temp = ind->weight_index->child;
	temp = del(temp);
	ind->nweight --;
	if (ind->nweight == 0)
	{
		ind->best_weight = -1;
	}

	return;
}

/* Routine to find undeveloped individuals (correspondingly their weights) in the archive set, 
 and if they are promising then add them into the evolutionary population */
int add_weights(ind_list *archive, population *pop, weightVectors *weight, int niche)
{
	int i, j, k;
	double **c;				// for recording the Euclidean distance between any two individuals in the archive set
	int *undeveloped;		// the index of the undeveloped individuals in the archive set
	int Nundeveloped;		// the number of the undeveloped individuals in the archive set
	ind_list *temp_ind;
	double distance, radius;
	int count;
	population *temp_pop;


	temp_pop = (population *)malloc(sizeof(population));
	allocate_memory_pop (temp_pop, archive_size);

	c = (double **)malloc(archive_size*sizeof(double*));  
	for (i=0; i<archive_size; i++)
    {
        c[i] = (double *)malloc(archive_size*sizeof(double));
    }	
	undeveloped = (int *)malloc(archive_size*sizeof(int)); 

// copy the archive individuals into a pop for ease of operation 	
	temp_ind = archive->child;
	i = 0;
	while (temp_ind != NULL)
	{
		copy_ind (temp_ind->ind, &(temp_pop->ind[i]));
		temp_ind = temp_ind->child;
		i++;
	}
 
//	normalise the poulation and the archive according to the archive
	normalize_bothpop(temp_pop, archive_size, pop, popsize);

//  calculate the Euclidean distance among individuals
	for (i=0; i<archive_size; i++)
	{   
		for(j=i+1; j<archive_size; j++)
		{
			distance = 0;
			for(k=0; k<nobj; k++)
			{
			    distance += (temp_pop->ind[i].obj_norm[k]-temp_pop->ind[j].obj_norm[k]) * (temp_pop->ind[i].obj_norm[k]-temp_pop->ind[j].obj_norm[k]);
			}
			distance = sqrt(distance);
            c[i][j] = distance;
            c[j][i] = distance;
		}
		c[i][i] = INF;
	}

//	set the radius for finding the undeveloped individuals
	if (archive_size == 1)
	{
		radius = 0;
	}
	else
	{
		radius = determine_radius(c, archive_size, 1);
	}
	
// find the undeveloped individuals of the archive
	Nundeveloped = 0;
	for (i=0; i<archive_size; i++)
	{   
		count = 0;					// record how many individuals (in the population) are located in this archive individual's niche
		for(j=0; j<popsize; j++)
		{
			distance = 0;
			for(k=0; k<nobj; k++)
			{
				distance += (temp_pop->ind[i].obj_norm[k] - pop->ind[j].obj_norm[k]) * (temp_pop->ind[i].obj_norm[k] - pop->ind[j].obj_norm[k]);
			}
			distance = sqrt(distance);
			if (distance <= radius)
			{
				count ++;
			}
		}
		if (count == 0)				// when the niche has no individual in the population
		{
			undeveloped[Nundeveloped] = i;
			Nundeveloped ++;
		}
	}


// first place the undeveloped individuals and their corresponding weight vectors into the population,
// and then check if they are promising, if not, then remove them
	j = weight_size;
	for (i=0; i<Nundeveloped; i++)
	{
		copy_ind(&temp_pop->ind[undeveloped[i]], &pop->ind[popsize]);		// add the undeveloped individual into the population
		while (weight[j].active == 1)
		{
			j++;
		}
// determine the weight vector of this undeveloped individual based on the TCH scalarization
		determine_weight(&weight[j], &temp_pop->ind[undeveloped[i]]);
// associate the weight vector with the undeveloped individual in the population
		associate_weightWithInd(weight, j, &pop->ind[popsize], popsize); 
// find and update the neighborhood of the weight vector, and in the meantime check if this weight vector is promising, if not, remove it
		findUpdate_neighborhood(weight, j, pop, niche);					   

		j++;
		popsize++;
	}


	free (undeveloped);
	for (i=0; i<archive_size; i++)
    {
        free (c[i]);
    }
    free (c);

	deallocate_memory_pop (temp_pop, archive_size);
    free (temp_pop);	
	return Nundeveloped;
}


/* Routine to remove weight vectors in the population until their number does not exceed weight_size */
void remove_weights(weightVectors *weight, population *pop)
{
	int i;
	int currentWeightSize;			// current weight vector size 
	int	currentIndSize;				// current individual size
	int index;						// index of the current worst individual


	currentWeightSize = 0;
	currentIndSize = 0;
	for (i=0; i<popsize; i++)
	{
		if (pop->ind[i].nweight > 0)
		{
			currentWeightSize += pop->ind[i].nweight;
			currentIndSize ++;
		}
	}
	
// remove the worst weight (in terms of scalarising function) that corresponds to an individual shared by the most weights in the population  
	while (currentWeightSize > weight_size)
	{
		index = 0;
		for (i=1; i<popsize; i++)
		{
			if (pop->ind[index].nweight < pop->ind[i].nweight)
			{
				index = i;
			}
			else
			{
				if (pop->ind[index].nweight == pop->ind[i].nweight && pop->ind[index].nweight != 0)
				{
					if (weight[pop->ind[index].weight_index->child->index].fvalue < weight[pop->ind[i].weight_index->child->index].fvalue)
					{
						index = i;
					}
				}
			}		
		}
		if (pop->ind[index].nweight == 1)
		{
			break;		// stop until any solution in the population corresponds to at most one weight
		}
		else
		{
			remove_weight(&weight[pop->ind[index].weight_index->child->index], &pop->ind[index]);
			currentWeightSize --;
		}
	}

// this is for the situation even when every solution in the population corresponds to only one weight, the number of the weights still exceeds the given weight size
	if (currentWeightSize > weight_size)
	{
// remove the most crowded solutions and their corresponding weight vectors
		maintain_pop(weight, pop, currentWeightSize);
	}

	return;
}


